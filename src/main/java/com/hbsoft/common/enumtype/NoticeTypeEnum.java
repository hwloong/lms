package com.hbsoft.common.enumtype;

import java.util.HashMap;
import java.util.Map;

public enum NoticeTypeEnum {

    NOTICE_TYPE_UNAUDIT("0", "未审核"),
    NOTICE_TYPE_AUDIT("1","已审核"),
    NOTICE_TYPE_REBUT("2","已驳回");


    private static Map<String, NoticeTypeEnum> nameMap = new HashMap<String, NoticeTypeEnum>(10);
    private static Map<String, NoticeTypeEnum> codeMap = new HashMap<String, NoticeTypeEnum>(10);

    static {
        NoticeTypeEnum[] allValues = NoticeTypeEnum.values();
        for (NoticeTypeEnum obj : allValues) {
            nameMap.put(obj.getName(), obj);
            codeMap.put(obj.getCode(), obj);
        }
    }

    private String name;
    private String code;

    private NoticeTypeEnum(String code, String name) {
        this.name = name;
        this.code = code;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public static NoticeTypeEnum parseByName(String name) {
        return nameMap.get(name);
    }

    public static NoticeTypeEnum parseByCode(String code) {
        return codeMap.get(code);
    }

    @Override
    public String toString() {
        return this.name;
    }
}
