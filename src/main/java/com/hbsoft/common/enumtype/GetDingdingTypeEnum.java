package com.hbsoft.common.enumtype;

import java.util.HashMap;
import java.util.Map;

public enum GetDingdingTypeEnum {

	GETDING_TYPE_ALL("1", "全部"),
	GETDING_TYPE_PART("2","部分");


	private static Map<String, GetDingdingTypeEnum> nameMap = new HashMap<String, GetDingdingTypeEnum>(10);
	private static Map<String, GetDingdingTypeEnum> codeMap = new HashMap<String, GetDingdingTypeEnum>(10);

	static {
		GetDingdingTypeEnum[] allValues = GetDingdingTypeEnum.values();
		for (GetDingdingTypeEnum obj : allValues) {
			nameMap.put(obj.getName(), obj);
			codeMap.put(obj.getCode(), obj);
		}
	}

	private String name;
	private String code;

	private GetDingdingTypeEnum(String code, String name) {
		this.name = name;
		this.code = code;

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public static GetDingdingTypeEnum parseByName(String name) {
		return nameMap.get(name);
	}

	public static GetDingdingTypeEnum parseByCode(String code) {
		return codeMap.get(code);
	}

	@Override
	public String toString() {
		return this.name;
	}
}
