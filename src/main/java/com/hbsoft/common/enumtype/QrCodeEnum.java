package com.hbsoft.common.enumtype;
import java.util.HashMap;
import java.util.Map;

public enum QrCodeEnum {

	QR_CODE_BANK("bank_","银行"),
	QR_CODE_BANKBRANCH("bankBranch_","部门单位"),
	QR_CODE_ACCOUNTMAMAGER("accountMamager_","客户经理");


	private static Map<String, QrCodeEnum> nameMap = new HashMap<String, QrCodeEnum>(10);
	private static Map<String, QrCodeEnum> codeMap = new HashMap<String, QrCodeEnum>(10);

	static {
		QrCodeEnum[] allValues = QrCodeEnum.values();
		for (QrCodeEnum obj : allValues) {
			nameMap.put(obj.getName(), obj);
			codeMap.put(obj.getCode(), obj);
		}
	}

	private String name;
	private String code;

	private QrCodeEnum(String code, String name) {
		this.name = name;
		this.code = code;

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public static QrCodeEnum parseByName(String name) {
		return nameMap.get(name);
	}

	public static QrCodeEnum parseByCode(String code) {
		return codeMap.get(code);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
