package com.hbsoft.common.enumtype;

import java.util.HashMap;
import java.util.Map;

public enum AreaUserRoleEnum {

	AREA_USER_ROLE_SHIB("1", "市办"),
	AREA_USER_ROLE_Bank("2","银行");


	private static Map<String, AreaUserRoleEnum> nameMap = new HashMap<String, AreaUserRoleEnum>(10);
	private static Map<String, AreaUserRoleEnum> codeMap = new HashMap<String, AreaUserRoleEnum>(10);

	static {
		AreaUserRoleEnum[] allValues = AreaUserRoleEnum.values();
		for (AreaUserRoleEnum obj : allValues) {
			nameMap.put(obj.getName(), obj);
			codeMap.put(obj.getCode(), obj);
		}
	}

	private String name;
	private String code;

	private AreaUserRoleEnum(String code, String name) {
		this.name = name;
		this.code = code;

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public static AreaUserRoleEnum parseByName(String name) {
		return nameMap.get(name);
	}

	public static AreaUserRoleEnum parseByCode(String code) {
		return codeMap.get(code);
	}

	@Override
	public String toString() {
		return this.name;
	}
}
