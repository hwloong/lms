package com.hbsoft.common.enumtype;

import java.util.HashMap;
import java.util.Map;

public enum MethodEnum {

	METHOD_ENUM_GET("GET", "GET"),
	METHOD_ENUM_POST("POST","POST");


	private static Map<String, MethodEnum> nameMap = new HashMap<String, MethodEnum>(10);
	private static Map<String, MethodEnum> codeMap = new HashMap<String, MethodEnum>(10);

	static {
		MethodEnum[] allValues = MethodEnum.values();
		for (MethodEnum obj : allValues) {
			nameMap.put(obj.getName(), obj);
			codeMap.put(obj.getCode(), obj);
		}
	}

	private String name;
	private String code;

	private MethodEnum(String code, String name) {
		this.name = name;
		this.code = code;

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public static MethodEnum parseByName(String name) {
		return nameMap.get(name);
	}

	public static MethodEnum parseByCode(String code) {
		return codeMap.get(code);
	}

	@Override
	public String toString() {
		return this.name;
	}
}
