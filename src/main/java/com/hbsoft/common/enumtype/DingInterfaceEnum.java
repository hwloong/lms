package com.hbsoft.common.enumtype;

import java.util.HashMap;
import java.util.Map;

public enum DingInterfaceEnum {

	DING_INTERFACE_ENUM_DING("1", "钉钉接口"),
	DING_INTERFACE_ENUM_HB("0","互邦接口");


	private static Map<String, DingInterfaceEnum> nameMap = new HashMap<String, DingInterfaceEnum>(10);
	private static Map<String, DingInterfaceEnum> codeMap = new HashMap<String, DingInterfaceEnum>(10);

	static {
		DingInterfaceEnum[] allValues = DingInterfaceEnum.values();
		for (DingInterfaceEnum obj : allValues) {
			nameMap.put(obj.getName(), obj);
			codeMap.put(obj.getCode(), obj);
		}
	}

	private String name;
	private String code;

	private DingInterfaceEnum(String code, String name) {
		this.name = name;
		this.code = code;

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public static DingInterfaceEnum parseByName(String name) {
		return nameMap.get(name);
	}

	public static DingInterfaceEnum parseByCode(String code) {
		return codeMap.get(code);
	}

	@Override
	public String toString() {
		return this.name;
	}
}
