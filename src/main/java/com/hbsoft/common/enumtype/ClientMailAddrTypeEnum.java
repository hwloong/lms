package com.hbsoft.common.enumtype;

import java.util.HashMap;
import java.util.Map;

public enum ClientMailAddrTypeEnum {
	
	CLIENTMAILADDRTYPE_DEFAULT("0","默认地址"),
	CLIENTMAILADDRTYPE_NO_DEFAULT("1","其他地址");	

	private static Map<String, ClientMailAddrTypeEnum> nameMap = new HashMap<String, ClientMailAddrTypeEnum>(10);
	private static Map<String, ClientMailAddrTypeEnum> codeMap = new HashMap<String, ClientMailAddrTypeEnum>(10);

	static {
		ClientMailAddrTypeEnum[] allValues = ClientMailAddrTypeEnum.values();
		for (ClientMailAddrTypeEnum obj : allValues) {
			nameMap.put(obj.getName(), obj);
			codeMap.put(obj.getCode(), obj);
		}
	}

	private String name;
	private String code;

	private ClientMailAddrTypeEnum(String code, String name) {
		this.name = name;
		this.code = code;

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public static ClientMailAddrTypeEnum parseByName(String name) {
		return nameMap.get(name);
	}

	public static ClientMailAddrTypeEnum parseByCode(String code) {
		return codeMap.get(code);
	}

	@Override
	public String toString() {
		return this.name;
	}
}
