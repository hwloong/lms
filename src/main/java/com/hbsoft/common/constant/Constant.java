package com.hbsoft.common.constant;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class Constant {

    public static String STARTCODE = "100";

    public static String ENDCODE = "001";

    public static String BANKAREA = "BANKAREA";

    public static String BANK = "BANK";

    public static String BANKBRANCH = "BANKBRANCH";

}
