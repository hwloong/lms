package com.hbsoft.lms.bean;

import java.util.Date;

import com.hb.annotation.FieldParam;
import com.hb.annotation.TableName;
import com.hb.bean.APojo;
import com.hb.hbenum.FieldType;
import lombok.Data;

@Data
@TableName("d_loanUse")
public class LoanUseBean extends APojo {

    @FieldParam(fieldType = FieldType.prikeyIncrement)
    private Integer id_prikey;

    @FieldParam("id")
    private String id;

    @FieldParam("name")
    private String name;
    
    @FieldParam("createBy")
    private String createBy;

    @FieldParam("createOn")
    private Date createOn;

    @FieldParam("updateBy")
    private String updateBy;
    
    @FieldParam("updateOn")
    private Date updateOn;
    
    @FieldParam("attribute")
    private String attribute;


}
