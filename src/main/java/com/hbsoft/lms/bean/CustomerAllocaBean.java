package com.hbsoft.lms.bean;

import com.alibaba.druid.sql.visitor.functions.Char;
import com.hb.annotation.FieldParam;
import com.hb.annotation.TableName;
import com.hb.bean.APojo;
import com.hb.hbenum.FieldType;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
@TableName("k_bespeak")
public class CustomerAllocaBean extends APojo {

    @FieldParam(fieldType = FieldType.prikey)
    private String id;

    @FieldParam("signaturePath")
    private String signaturePath;

    @FieldParam("idNumberRecognitionId")
    private String idNumberRecognitionId;

    @FieldParam("FastFlag")
    private Integer FastFlag;

    @FieldParam("type")
    private Integer type;

    @FieldParam("userId")
    private String userId;

    @FieldParam("name")
    private String name;

    @FieldParam("idNumber")
    private String idNumber;

    @FieldParam("mobile")
    private String mobile;

    @FieldParam("address")
    private String address;

    @FieldParam("mainBusiness")
    private String mainBusiness;

    @FieldParam("loanUse")
    private String loanUse;

    @FieldParam("creditCode")
    private String creditCode;

    @FieldParam("legalMobile")
    private String legalMobile;

    @FieldParam("accountingMobile")
    private String accountingMobile;

    @FieldParam("bankcode")
    private String bankcode;

    @FieldParam("dotId")
    private String dotId;

    @FieldParam("deptId")
    private Integer deptId;

    @FieldParam("unit")
    private String unit;

    @FieldParam("amount")
    private Float amount;

    @FieldParam("createDate")
    private Date createDate;

    @FieldParam("examineDate")
    private String examineDate;

    @FieldParam("loanDate")
    private String loanDate;

    @FieldParam("rate")
    private Float rate;

    @FieldParam("deactivate")
    private Integer deactivate;

    @FieldParam("flag")
    private Integer flag;

    @FieldParam("oldamount")
    private Float oldamount;

    @FieldParam("oldrate")
    private Float oldrate;

    @FieldParam("urgentFlag")
    private Integer urgentFlag;

    @FieldParam("urgentDate")
    private String urgentDate;

    @FieldParam("urgentAllotTime")
    private Date urgentAllotTime;

    @FieldParam("intention")
    private String intention;

    @FieldParam("notes")
    private String notes;

    @FieldParam("finding")
    private String finding;

    @FieldParam("findingnotes")
    private String findingnotes;

    @FieldParam("geoLat")
    private BigDecimal geoLat;

    @FieldParam("geoLong")
    private BigDecimal geoLong;

    @FieldParam("geoAddress")
    private String geoAddress;

    @FieldParam("assister")
    private String assister;

    @FieldParam("pattern")
    private String pattern;

    @FieldParam("patternExt")
    private String patternExt;

    @FieldParam("reason")
    private String reason;

    @FieldParam("creditAmount")
    private Float creditAmount;

    @FieldParam("usedAmount")
    private Float usedAmount;

    @FieldParam("usedMonth")
    private Integer usedMonth;

    @FieldParam("payMode")
    private String payMode;

    @FieldParam("entrustedAmount")
    private Float entrustedAmount;

    @FieldParam("handleDate")
    private Date handleDate;

    @FieldParam("handleBank")
    private String handleBank;

    @FieldParam("acceptNotes")
    private String acceptNotes;

    @FieldParam("agreement")
    private String agreement;

    @FieldParam("cardNo")
    private String cardNo;

    @FieldParam("contractNo")
    private String contractNo;

    @FieldParam("conductNotes")
    private String conductNotes;

    @FieldParam("intentionTimeout")
    private Date intentionTimeout;

    @FieldParam("intentionTime")
    private Date intentionTime;

    @FieldParam("examineStartTime")
    private Date examineStartTime;

    @FieldParam("examineEndTime")
    private Date examineEndTime;

    @FieldParam("reviewStartTime")
    private Date reviewStartTime;

    @FieldParam("reviewEndTime")
    private Date reviewEndTime;

    @FieldParam("gradeEndTime")
    private Date gradeEndTime;

    @FieldParam("creditStartTime")
    private Date creditStartTime;

    @FieldParam("creditUsedTime")
    private Date creditUsedTime;

    @FieldParam("toAuthLoanTime")
    private Date toAuthLoanTime;

    @FieldParam("acceptTime")
    private Date acceptTime;

    @FieldParam("conductTime")
    private Date conductTime;

    @FieldParam("makeLoanTime")
    private Date makeLoanTime;

    @FieldParam(value = "流程id",association = true)
    private String flowId;

    @FieldParam(value = "当前流程",association = true)
    private String flowName;

    @FieldParam(value = "贷前经理",association = true)
    private String BeforeLend;

    @FieldParam(value = "贷中经理",association = true)
    private String BeforeCenter;

    @FieldParam(value = "贷后经理",association = true)
    private String BeforeBack;

    @FieldParam(value = "客户经理分组",association = true)
    private String accountManagerGroup;

    @FieldParam(value = "贷前经理id",association = true)
    private String beforeManagerId;

    @FieldParam(value = "贷中经理id",association = true)
    private String authManagerId;

    @FieldParam(value = "贷后经理id",association = true)
    private String afterManagerId;
}
