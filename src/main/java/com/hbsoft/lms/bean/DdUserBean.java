package com.hbsoft.lms.bean;

import com.hb.annotation.FieldParam;
import com.hb.annotation.TableName;
import com.hb.bean.APojo;
import com.hb.hbenum.FieldType;
import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@TableName("dd_user")
public class DdUserBean extends APojo {

    @NotNull(message = "主键参数有误")
    @FieldParam(fieldType = FieldType.prikeyIncrement)
    private Integer id_prikey;

    @FieldParam("userid")
    private String userid ;

    @FieldParam("name")
    private String name ;

    @FieldParam("mobile")
    private String mobile ;

    @FieldParam("deptId")
    private String deptId;

    @FieldParam(association = true, forkeyId = "deptId", forkeyName = "name", forkeyRelationId = "id", forkeyTableName = "DD_DEPT")
    private String  deptName;

    @FieldParam("position")
    private String position;

    @FieldParam("avatar")
    private String avatar;

    @FieldParam("active")
    private String active ;

    @FieldParam("isAdmin")
    private String isAdmin ;

    @FieldParam("isBoss")
    private String isBoss ;

    @FieldParam("isHide")
    private String isHide ;

    @FieldParam("isLeader")
    private String isLeader ;

    @FieldParam("unionid")
    private String unionid ;

    @FieldParam("order")
    private Integer order ;

    @FieldParam("ryid")
    private String ryid;

    @FieldParam("jobnumber")
    private String jobnumber;

    @FieldParam("noview")
    private Integer noview ;

//    @NotBlank(message = "请选择客户经理类别")
    @FieldParam("userType")
    private String userType ;

//    @NotBlank(message = "请选择客户经理分组")
    @FieldParam("userItem")
    private String userItem ;

//    @NotBlank(message = "请选择客户经理分组")
    @FieldParam("USERITEMSTR")
    private String userItemStr;

    @FieldParam("userLarge")
    private Integer userLarge ;

    @FieldParam("recommend")
    private Integer recommend ;

    @FieldParam("showmobile")
    private String showmobile ;

    @FieldParam("leaveFlag")
    private Integer leaveFlag;

    @FieldParam("loanUseIds")
    private String loanUseIds;

    @FieldParam("LOANUSEIDSTR")
    private String loanUseIdstr;

    @FieldParam("handleNum")
    private Integer handleNum;

    @FieldParam("dzb")
    private String dzb;

    @FieldParam("dzbrole")
    private String dzbrole;

    @FieldParam("bankCode")
    private String bankCode;

    @FieldParam("updateOn")
    private Date updateOn;

    @FieldParam("updateBy")
    private String updateBy;
}
