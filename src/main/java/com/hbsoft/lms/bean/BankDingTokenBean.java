package com.hbsoft.lms.bean;

import java.util.Date;

import com.hb.annotation.FieldParam;
import com.hb.annotation.TableName;
import com.hb.bean.APojo;
import com.hb.hbenum.FieldType;

import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

@Data
@TableName("D_BankDingToken")
public class BankDingTokenBean extends APojo {

	@FieldParam(fieldType = FieldType.prikeyIncrement)
	private Integer id_prikey;
	
	@FieldParam("BankCode")
	private String bankCode;

	@NotBlank(message = "请输入钉钉企业id")
	@FieldParam("CorpID")
	private String corpID;
	
	@FieldParam("TokenMode")
	private String tokenMode;

	@NotBlank(message = "请输入钉钉应用id")
	@FieldParam("AgentID")
	private String agentID;
	
	@FieldParam("AppKey")
	private String appKey;
	
	@FieldParam("AppSecret")
	private String appSecret;
	
	@FieldParam("ServiceMethod")
	private String serviceMethod;
	
	@FieldParam("ServiceURL")
	private String serviceURL;
	
	@FieldParam("ServiceParas")
	private String serviceParas;

	@FieldParam("createBy")
	private String createBy;

	@FieldParam("createOn")
	private Date createOn;

	@FieldParam("updateBy")
	private String updateBy;

	@FieldParam("updateOn")
	private Date updateOn;

	@FieldParam("getDdType")
	private String getDdType;

	@FieldParam("ddDeptId")
	private String ddDeptId;
}
