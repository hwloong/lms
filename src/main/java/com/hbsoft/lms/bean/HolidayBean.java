package com.hbsoft.lms.bean;

import com.hb.annotation.FieldParam;
import com.hb.annotation.TableName;
import com.hb.bean.APojo;
import com.hb.hbenum.FieldType;
import lombok.Data;

@Data
@TableName("D_Holiday")
public class HolidayBean extends APojo {

    private static final long serialVersionUID = 1L;

    @FieldParam(fieldType = FieldType.prikeyIncrement)
    private Integer id_prikey;

    @FieldParam(value = "节日名称",association = true)
    private String name;

    @FieldParam(value = "week",association = true)
    private String week;

    @FieldParam("holiday")
    private String holiday;

    @FieldParam("type")
    private String type;

    @FieldParam(value = "开始时间",association = true)
    private String startTime;

    @FieldParam(value = "结束时间",association = true)
    private String endTime;
}
