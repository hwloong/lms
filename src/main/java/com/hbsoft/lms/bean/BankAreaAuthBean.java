package com.hbsoft.lms.bean;

import com.hb.annotation.FieldParam;
import com.hb.annotation.TableName;
import com.hb.bean.APojo;
import com.hb.hbenum.FieldType;
import lombok.Data;

@Data
@TableName("k_bank_user")
public class BankAreaAuthBean extends APojo {

    @FieldParam(fieldType = FieldType.prikeyIncrement)
    private Integer id_prikey;

    @FieldParam("bankcode")
    private String bankcode;

    @FieldParam("areacode")
    private String areacode;

    @FieldParam("type")
    private String type;

    @FieldParam("userId")
    private String userId;
}
