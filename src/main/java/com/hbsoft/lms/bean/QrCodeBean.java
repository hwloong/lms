package com.hbsoft.lms.bean;

import com.hb.annotation.FieldParam;
import com.hb.annotation.TableName;
import com.hb.bean.APojo;
import com.hb.hbenum.FieldType;
import lombok.Data;

import java.util.Date;

@Data
@TableName("k_qrcode")
public class QrCodeBean extends APojo {

    @FieldParam(fieldType = FieldType.prikeyIncrement)
    private Integer id_prikey;

    @FieldParam("type")
    private String type;

    @FieldParam("id")
    private String id;

    @FieldParam("ticket")
    private String ticket;

    @FieldParam("url")
    private String url;

    @FieldParam("path")
    private String path;

    @FieldParam("createOn")
    private Date createOn;

    private String name;



}
