package com.hbsoft.lms.bean;

import com.hb.annotation.FieldParam;
import com.hb.annotation.TableName;
import com.hb.bean.APojo;
import com.hb.hbenum.FieldType;
import lombok.Data;

import java.util.Date;

@Data
@TableName("K_Notice")
public class NoticeBean extends APojo {

    private static final long serialVersionUID = 1L;

    @FieldParam(value = "ID_PRIKEY", fieldType = FieldType.prikeyIncrement)
    private Integer id_prikey;

    @FieldParam("标题")
    private String title;

    @FieldParam("内容")
    private String content;

    @FieldParam("发布人")
    private Integer issId;

    @FieldParam(value = "发布人名字", association = true)
    private String issName;

    @FieldParam("发布时间")
    private Date issueTime;

    @FieldParam("到期日期")
    private String matureTime;

    @FieldParam("审核人")
    private Integer auditId;

    @FieldParam(value = "审核状态详情", association = true)
    private String status;

    //0 已审核 1 未审核 2已驳回
    @FieldParam("审核状态")
    private Integer state;

    @FieldParam(value = "审核人名字", association = true)
    private String auditName;

    @FieldParam("审核时间")
    private Date autiTime;

    @FieldParam("备注")
    private String remark;

    @FieldParam(value = "开始时间",association = true)
    private String startDate;

    @FieldParam(value = "结束时间",association = true)
    private String endDate;
}
