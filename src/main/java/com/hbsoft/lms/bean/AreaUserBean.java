package com.hbsoft.lms.bean;

import com.hb.annotation.FieldParam;
import com.hb.annotation.TableName;
import com.hb.bean.APojo;
import com.hb.hbenum.FieldType;
import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

import java.util.Date;

@Data
@TableName("k_area_user_rel")
public class AreaUserBean extends APojo {

    @FieldParam(fieldType = FieldType.prikeyIncrement)
    private Integer id_prikey;

    @FieldParam("区域编号")
    private String code;

    @NotBlank(message = "请输入姓名")
    @FieldParam("姓名")
    private String name;

    @NotBlank(message = "请输入手机号码")
    @FieldParam("手机号码")
    private String tel;

    @FieldParam("钉钉部门编号")
    private String ddDetpId;

    @NotBlank(message = "请输入密码")
    @FieldParam(association = true)
    private String password;

    @FieldParam("角色")
    private String roleType;

    @FieldParam("bankCode")
    private String bankCode;

    @FieldParam("创建者")
    private String createBy;

    @FieldParam("创建时间")
    private Date  createOn ;

    @FieldParam("更新者")
    private String updateBy;

    @FieldParam("更新时间")
    private Date updateOn;

    @FieldParam(association = true)
    private String id;

    @FieldParam(value = "区域名称",association = true)
    private String AreaName;

}
