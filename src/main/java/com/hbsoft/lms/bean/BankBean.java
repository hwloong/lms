package com.hbsoft.lms.bean;

import com.hb.annotation.FieldParam;
import com.hb.annotation.TableName;
import com.hb.bean.APojo;
import com.hb.hbenum.FieldType;
import com.hb.hbenum.TreeType;
import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

@Data
@TableName("d_bank")
public class BankBean extends APojo {

    @FieldParam(fieldType = FieldType.prikeyIncrement)
    private Integer id_prikey;

    @FieldParam("代码")
    private String code;

    @NotBlank(message = "请输入名称")
    @FieldParam("项目")
    private String name;

    @FieldParam("备注")
    private String remark;

    @FieldParam("顺序")
    private Integer sort;

    @FieldParam("区域编号")
    private String areaCode;

    @FieldParam(association = true, forkeyId = "区域编号", forkeyName = "项目", forkeyRelationId = "代码", forkeyTableName = "d_bank_area")
    private String areaName;

    @NotBlank(message = "请输入银行编号")
    @FieldParam("bankcode")
    private String bankCode;

    @FieldParam(association = true, forkeyId = "bankCode", forkeyName = "path", forkeyRelationId = "id", forkeyTableName = "k_qrcode")
    private String bankQrcodePath;


    @FieldParam("类型")
    private Integer bankType;

    @FieldParam("地址")
    private String bankAddress;

    @FieldParam("经度")
    private String longitude;

    @FieldParam("纬度")
    private String latitude;

    @FieldParam("范围")
    private Integer scope;

    @FieldParam("客服热线")
    private String customerServiceTel;

    @FieldParam("银行全称")
    private String bankName;

    @FieldParam("创建时间")
    private Date createOn;

    @FieldParam("创建者")
    private String createBy;

    @FieldParam("更新时间")
    private Date updateOn;

    @FieldParam("更新者")
    private String updateBy;

    @FieldParam(association = true, treeType = TreeType.pId, treePidSubstring = true, treePidSubstringLength = 3)
    private String pId;

    private boolean isParent;

    private String url;

}
