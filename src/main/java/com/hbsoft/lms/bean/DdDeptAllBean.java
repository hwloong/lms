package com.hbsoft.lms.bean;

import com.hb.annotation.FieldParam;
import com.hb.annotation.TableName;
import com.hb.bean.APojo;
import com.hb.hbenum.FieldType;
import lombok.Data;

import java.util.Date;

@Data
@TableName("DD_DEPT_ALL")
public class DdDeptAllBean extends APojo {

    @FieldParam(fieldType = FieldType.prikeyIncrement)
    private Integer id_prikey;

    @FieldParam("id")
    private String id;

    @FieldParam("name")
    private String name;

    @FieldParam(association = true)
    private String parentid;

    @FieldParam("pid")
    private String pid;

    @FieldParam("bankCode")
    private String bankCode;

    @FieldParam("updateOn")
    private Date updateOn;

    @FieldParam(association = true)
    private String flag;

}
