package com.hbsoft.lms.bean;

import com.hb.annotation.FieldParam;
import com.hb.annotation.TableName;
import com.hb.bean.APojo;
import com.hb.hbenum.FieldType;
import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

@Data
@TableName("DD_USER_ITEM")
public class BankBranchBean extends APojo {

    @FieldParam(fieldType = FieldType.prikeyIncrement)
    private Integer id_prikey;

    @FieldParam("代码")
    private String code;

    @NotBlank(message = "请输入名称")
    @FieldParam("项目")
    private String name;

    @FieldParam("备注")
    private String remark;

    @FieldParam("顺序")
    private Integer sort;

    @FieldParam("区域编号")
    private String areaCode;

    @FieldParam("bankcode")
    private String bankCode;

    @FieldParam("电话")
    private String tel;

    @FieldParam("地址")
    private String bankAddress;

    @FieldParam("dotlong")
    private String dotlong;

    @FieldParam("dotlat")
    private String dotlat;

    @FieldParam("范围")
    private Integer scope;

    @FieldParam("个人贷款标识")
    private String personalLoanFlag;

    @FieldParam("个人贷款起始金额")
    private BigDecimal personalLoanStart;

    @FieldParam("个人贷款结束金额")
    private BigDecimal personalLoanEnd;

    @FieldParam("企业贷款标识")
    private String companyLoanFlag;

    @FieldParam("企业贷款起始金额")
    private BigDecimal companyLoanStart;

    @FieldParam("企业贷款结束金额")
    private BigDecimal companyLoanEnd;

    @FieldParam("创建时间")
    private Date createOn;

    @FieldParam("创建者")
    private String createBy;

    @FieldParam("更新时间")
    private Date updateOn;

    @FieldParam("更新者")
    private String updateBy;

}
