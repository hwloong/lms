package com.hbsoft.lms.bean;

import com.hb.annotation.FieldParam;
import com.hb.annotation.TableName;
import com.hb.bean.APojo;
import com.hb.hbenum.FieldType;
import com.hb.hbenum.TreeType;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@TableName("d_bank_area")
public class BankAreaBean extends APojo {

    @FieldParam(fieldType = FieldType.prikeyIncrement)
    private Integer id_prikey;

    @FieldParam("代码")
    private String code;

    @NotNull(message = "请输入名称")
    @FieldParam("项目")
    private String name;

    @FieldParam("备注")
    private String remark;

    @FieldParam("顺序")
    private Integer sort;

    @FieldParam("经度")
    private String longitude;

    @FieldParam("纬度")
    private String latitude;

    @FieldParam("范围")
    private Integer scope;

    @FieldParam("创建时间")
    private Date createOn;

    @FieldParam("创建者")
    private String createBy;

    @FieldParam("更新时间")
    private Date updateOn;

    @FieldParam("更新者")
    private String updateBy;

    @FieldParam(association = true, treeType = TreeType.pId, treePidSubstring = true, treePidSubstringLength = 3)
    private String pId;

    @FieldParam(value = "是否父ID",association = true)
    private boolean isParent;

}
