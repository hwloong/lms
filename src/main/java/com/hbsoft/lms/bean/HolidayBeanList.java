package com.hbsoft.lms.bean;

import lombok.Data;

import java.util.List;

@Data
public class HolidayBeanList {

    private List<HolidayBean> holidayList;
}
