package com.hbsoft.lms.vo;

import lombok.Data;

@Data
public class TreeVo {

    private String name;

    private String id;

    private String pId;
}
