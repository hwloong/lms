package com.hbsoft.lms.vo;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Scene {
	 private String scene_id;

	 private String scene_str;
}
