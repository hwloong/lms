package com.hbsoft.lms.vo;

import lombok.Data;

@Data
public class DictVo {

    private String code;

    private String name;

}
