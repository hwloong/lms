package com.hbsoft.lms.vo;

import com.hbsoft.lms.bean.DdDeptBean;
import lombok.Data;

import java.util.List;

@Data
public class DepartmentVo {

    private String errcode;

    private String errmsg;

    private List<DdDeptBean> department;
}
