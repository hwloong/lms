package com.hbsoft.lms.vo;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class QrCode {
	 
	 private String action_name;
	 
	 private Action_info action_info;
}
