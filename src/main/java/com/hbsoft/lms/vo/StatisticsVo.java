package com.hbsoft.lms.vo;

import com.hb.bean.APojo;
import lombok.Data;

@Data
public class StatisticsVo extends APojo{

    String areaCode;
    String bankCode;
}
