package com.hbsoft.lms.vo;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Action_info {

	private Scene scene;
}
