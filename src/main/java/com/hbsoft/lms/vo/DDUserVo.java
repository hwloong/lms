package com.hbsoft.lms.vo;

import com.hbsoft.lms.bean.DdDeptBean;
import com.hbsoft.lms.bean.DdUserBean;
import lombok.Data;

import java.util.List;

@Data
public class DDUserVo {

    private String errcode;

    private String errmsg;

    private List<DdUserBean> userlist;
}
