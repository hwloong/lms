package com.hbsoft.lms.controller;

import com.hb.bean.CallResult;
import com.hb.controller.ABaseController;
import com.hbsoft.lms.bean.HolidayBean;
import com.hbsoft.lms.bean.HolidayBeanList;
import com.hbsoft.lms.dao.service.HolidayBeanDaoService;
import com.hbsoft.util.HolidayUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "", produces = "application/json;charset=UTF-8")
public class HolidayController extends ABaseController {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    HolidayBeanDaoService holidayBeanDaoService;

    /**
     * 接口获取指定时间段节假日
     * @param holidayBean
     * @return
     */
    @PostMapping("getHoliday")
    public String getHoliday(HolidayBean holidayBean) {
        Map<String, Object> map = new HashMap<>();
        logger.info("获取指定时间段节假日开始");
        try {
            List<String> days = HolidayUtil.getAllDays(holidayBean.getStartTime(),holidayBean.getEndTime());
            List<Map<String, Object>> list = HolidayUtil.holidayCopy(days);

            if (null != list && list.size() > 0) {
                addBatchHoliday(list);
                map = holidayBeanDaoService.getPagingData(holidayBean);
            }
        }catch (Exception e) {
            map.put("code",400);
            map.put("msg","获取失败");
            e.printStackTrace();
        }
        logger.info("获取指定时间段节假日结束");
        return gson.toJson(map);
    }

    /**
     * 获取已设置的节假日
     * @param holidayBean
     * @return
     */
    @PostMapping("getHolidayDsik")
    public String getHolidayDsik(HolidayBean holidayBean) {
        Map<String, Object> map = null;
        logger.info("获取已设置的节假日开始");
        try{
            map = holidayBeanDaoService.getPagingData(holidayBean);
        }catch (Exception e) {
            map = new HashMap<>();
            map.put("code",400);
            map.put("msg","获取失败");
            e.printStackTrace();
        }
        logger.info("获取已设置的节假日结束");
        return gson.toJson(map);
    }

    /**
     * 添加节假日
     * @param holidayBean
     * @return
     */
    @PostMapping("addHoliday")
    public String addHoliday(HolidayBean holidayBean) {
        CallResult<String> result = new CallResult<>();
        logger.info("添加节假日开始");
        try {
            HolidayBean holidayBean1 = holidayBeanDaoService.getByField(holidayBean);
            if (null == holidayBean1) {
                holidayBeanDaoService.add(holidayBean);
                result.setCode(0);
                result.setMessage("添加成功");
            }else {
                result.setCode(400);
                result.setMessage("节假日已经存在");
            }

        }catch (Exception e) {
            result.setCode(400);
            result.setMessage("添加失败");
            e.printStackTrace();
        }
        logger.info("添加结束");
        return gson.toJson(result);
    }

    /**
     * 批量添加节假日
     * @param holidayList
     * @return
     */
    @PostMapping("addHolidayList")
    public String addHolidayList(HolidayBeanList holidayList) {
        CallResult<String> result = new CallResult<>();
        logger.info("批量添加节假日");
        try {
            List<HolidayBean> list = holidayList.getHolidayList();
            if (null != list && list.size() > 0) {
                for (int i=0; i<list.size(); i++) {
                    HolidayBean holidayBean = holidayBeanDaoService.getByField(list.get(i));
                    if (null != holidayBean) {
                        list.get(i).setId_prikey(holidayBean.getId_prikey());
                        holidayBeanDaoService.set(list.get(i));
                    }else {
                        holidayBeanDaoService.add(list.get(i));
                    }
                }
                result.setCode(0);
                result.setMessage("设置成功");
            }else {
                result.setCode(400);
                result.setMessage("没有需要添加的节假日");
            }

        }catch (Exception e) {
            result.setCode(400);
            result.setMessage("设置失败");
            e.printStackTrace();
        }
        logger.info("批量添加结束");
        return gson.toJson(result);
    }

    public void addBatchHoliday(List<Map<String, Object>> list) throws Exception {
        if ((null != list && list.size() > 0)) {
            for (int i=0; i<list.size(); i++) {
                HolidayBean holidayBean = new HolidayBean();
                holidayBean.setType((String)list.get(i).get("type"));
                holidayBean.setHoliday((String)list.get(i).get("holiday"));
                HolidayBean holidayBean1 = holidayBeanDaoService.getByField(holidayBean);
                if (null == holidayBean1) {
                    holidayBeanDaoService.add(holidayBean);
                }
            }
        }
    }

    /**
     * 删除节假日
     * @param holidayBean
     * @return
     */
    @PostMapping("deleteHoliday")
    public String deleteHoliday(HolidayBean holidayBean) {
        CallResult<String> result = new CallResult<>();
        logger.info("删除节假日开始");
        try {
            if (null == holidayBean.getId_prikey()) {
                result.setCode(400);
                result.setMessage("主键不能为空");
                return gson.toJson(result);
            }
            holidayBeanDaoService.removeOne(holidayBean.getId_prikey());
            result.setCode(0);
            result.setMessage("删除成功");
        }catch (Exception e) {
            result.setCode(400);
            result.setMessage("删除失败失败");
            e.printStackTrace();
        }
        logger.info("删除节假日结束");
        return gson.toJson(result);
    }

    /**
     * 修改节假日
     * @param holidayBean
     * @return
     */
    @PostMapping("updateHoliday")
    public String updateHoliday(HolidayBean holidayBean) {
        CallResult<String> result = new CallResult<>();
        logger.info("修改节假日开始");
        try {
            if (null == holidayBean.getId_prikey()) {
                result.setCode(400);
                result.setMessage("主键不能为空");
                return gson.toJson(result);
            }
            holidayBeanDaoService.set(holidayBean);
            result.setCode(0);
            result.setMessage("修改成功");
        }catch (Exception e) {
            result.setCode(400);
            result.setMessage("删除失败失败");
            e.printStackTrace();
        }
        logger.info("修改节假日结束");
        return gson.toJson(result);
    }
}
