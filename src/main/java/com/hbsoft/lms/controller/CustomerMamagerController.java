package com.hbsoft.lms.controller;

import com.hb.bean.CallResult;
import com.hb.controller.ABaseController;
import com.hbsoft.common.enumtype.AreaUserRoleEnum;
import com.hbsoft.lms.bean.AreaUserBean;
import com.hbsoft.lms.bean.BankBean;
import com.hbsoft.lms.bean.DdUserBean;
import com.hbsoft.lms.dao.service.*;
import com.hbsoft.lms.service.CustomerManagerService;
import com.hbsoft.util.GsonUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(value = "", produces = "application/json;charset=UTF-8")
public class CustomerMamagerController extends ABaseController {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    DdUserBeanDaoService ddUserBeanDaoService;

    @Autowired
    BankBranchBeanDaoService bankBranchBeanDaoService;

    @Autowired
    LoanUseBeanDaoService loanUseBeanDaoService;

    @Autowired
    CustomerManagerService customerManagerService;

    @Autowired
    private AreaUserBeanDaoService areaUserBeanDaoService;

    @Autowired
    private BankBeanDaoService bankBeanDaoService;

    @RequestMapping("getAccountMamagerList")
    public String getAccountMamagerList(DdUserBean ddUserBean){
        Map<String,Object> map = new HashMap<>();
        try {
            CallResult<String> bankCodeResult = getBankCodeByLogin(AreaUserRoleEnum.AREA_USER_ROLE_Bank.getCode());
            if (!bankCodeResult.isExec()) {
                return GsonUtil.toJson(bankCodeResult);
            }
            if(ddUserBean.getUserItem()!= null && ddUserBean.getUserItem().length() == 6){
                ddUserBean.setUserItem(null);
            }
            ddUserBean.setBankCode(bankCodeResult.getData());
            map =  ddUserBeanDaoService.getPagingData(ddUserBean);
        } catch (Exception e) {
            e.printStackTrace();
            map.put("code",400);
            map.put("msg","获取失败");
        }
        return GsonUtil.toJson(map);
    }


    @RequestMapping("updateAccountMamager")
    public String updateAccountMamager(@Validated DdUserBean ddUserBean){
        CallResult<String> result = new CallResult<>();
        try {
            ddUserBeanDaoService.set(ddUserBean);
            result.setCode(0);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            result.setCode(400);
            result.setMsg("操作失败");
        }
        return  GsonUtil.toJson(result);
    }

    @RequestMapping("updateDdInfo")
    public String updateDdInfo(String deptIdParam){
        CallResult<String> result = new CallResult<>();
        try {
            CallResult<String> bankCodeResult = getBankCodeByLogin(AreaUserRoleEnum.AREA_USER_ROLE_Bank.getCode());
            if (!bankCodeResult.isExec()) {
                return GsonUtil.toJson(bankCodeResult);
            }
            result = customerManagerService.updateDdInfo(bankCodeResult.getData(),deptIdParam);
        } catch (Exception e) {
            e.printStackTrace();
            result.setCode(400);
            result.setMsg("操作失败");
            logger.info(e.getMessage(),e);
        }
        return GsonUtil.toJson(result);
    }


    public CallResult<String> getBankCodeByLogin(String roleType) throws Exception {
        CallResult<String> result = new CallResult<>();
        AreaUserBean param = new AreaUserBean();
        param.setRoleType(roleType);
        param.setTel(hb_user.getName());
        AreaUserBean au = areaUserBeanDaoService.getByField(param);
        if (au == null || au.getCode() == null || "".equals(au.getCode())) {
            result.setCode(400);
            result.setMsg("权限不足");
            return result;
        } else {
            BankBean bbparam = new BankBean();
            bbparam.setCode(au.getCode());
            BankBean bankBean = bankBeanDaoService.getByField(bbparam);
            if (bankBean != null) {
                result.setData(bankBean.getBankCode());
            }
            result.setCode(0);
            result.setMsg("操作成功");
        }
        return result;
    }
}
