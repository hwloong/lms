package com.hbsoft.lms.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hb.bean.CallResult;
import com.hb.controller.ABaseController;
import com.hbsoft.lms.bean.BankBean;
import com.hbsoft.lms.dao.service.BankBeanDaoService;
import com.hbsoft.lms.service.StatisticsService;
import com.hbsoft.lms.vo.StatisticsVo;

@RestController
public class StatisticsController extends ABaseController {

	@Autowired
	StatisticsService statisticsService;
	@Autowired
	BankBeanDaoService bankBeanDaoService;

	@PostMapping("getAreaCodeByName")
	public String getAreaCodeByName(){
//		18600457998
		CallResult<String> result = new CallResult<>();
		if(isEmpty(hb_user.getName())){
			result.setCode(400);
			result.setMessage("请重新登录");
			return gson.toJson(result);
		}
		try {
			String areaCode = statisticsService.getAreaCodeByName(hb_user.getName());
			result.setData(areaCode);
			result.setCode(0);
			result.setMessage("获取成功");
		} catch (Exception e) {
			e.printStackTrace();
			result.setCode(400);
			result.setMessage("操作失败");
		}
		return gson.toJson(result);
	}
	/**
	 * 统计 预约人、银行、区域、客户经理、区域管理员、银行管理员总数
	 * 
	 * @return
	 */
	@PostMapping("statisticsCount")
	public String statisticsCount(StatisticsVo vo) {
		CallResult<Map<String, Object>> result = new CallResult<>();
		try {
			Map<String, Object> statisticsCount = statisticsService.statisticsCount(vo);
			result.setCode(0);
			result.setData(statisticsCount);
			result.setMessage("操作成功");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			result.setCode(400);
			result.setMessage("操作失败");
			e.printStackTrace();
		}
		return gson1.toJson(result);
	}

	/**
	 * 预约人贷前、贷中、贷后总数
	 * 
	 * @return
	 */
	@PostMapping("bespeakFlow")
	public String bespeakFlow(StatisticsVo vo) {
		CallResult<Map<String, Object>> result = new CallResult<>();
		try {
			Map<String, Object> bespeakFlow = statisticsService.bespeakFlow(vo);
			result.setCode(0);
			result.setMessage("获取成功");
			result.setData(bespeakFlow);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			result.setCode(400);
			result.setMessage("获取失败");
			e.printStackTrace();
		}
		return gson1.toJson(result);

	}


	/**
	 * 预约人城市贷前、贷中、贷后排名
	 * 
	 * @return
	 */
	@PostMapping("bespeakFlowCityRank")
	public String bespeakFlowCityRank(StatisticsVo vo) {
		CallResult<List<Map<String, Object>>> result = new CallResult<>();
		Integer start;
		Integer end;
		try {
			List<Map<String, Object>> list = statisticsService.bespeakFlowCityRank();
			if (list != null) {
				result.setCount(list.size());
			}
			if(list.size()<vo.getEnd()){
				end=list.size();
			}else{
				end = vo.getEnd();
			}
			if(list.size()<vo.getStart()){
				result.setCode(0);
				result.setMessage("获取成功");
				return gson1.toJson(result);
			}else{
				start = vo.getStart();
			}			
			List<Map<String,Object>> subList = list.subList(start,end);
			result.setCode(0);
			result.setMessage("获取成功");
			result.setData(subList);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			result.setCode(400);
			result.setMessage("获取失败");
			e.printStackTrace();
		}

		return gson1.toJson(result);

	}

	/**
	 * 预约人银行 贷前、贷中、贷后排名
	 * 
	 * @return
	 */
	@PostMapping("bespeakFlowBankRank")
	public String bespeakFlowBankRank(StatisticsVo vo) {
		CallResult<List<Map<String, Object>>> result = new CallResult<>();
		Integer start;
		Integer end;
		try {
			List<Map<String, Object>> list = statisticsService.bespeakFlowBankRank(vo);
			if (list != null) {
				result.setCount(list.size());
			}
			if(list.size()<vo.getEnd()){
				end=list.size();
			}else{
				end = vo.getEnd();
			}
			if(list.size()<vo.getStart()){
				result.setCode(0);
				result.setMessage("获取成功");
				return gson1.toJson(result);
			}else{
				start = vo.getStart();
			}			
			List<Map<String,Object>> subList = list.subList(start,end);
			result.setCode(0);
			result.setMessage("获取成功");
			result.setData(subList);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			result.setCode(400);
			result.setMessage("获取失败");
			e.printStackTrace();
		}

		return gson1.toJson(result);

	}

	/**
	 * 客户经理人数 银行排名
	 * 
	 * @return
	 */
	@PostMapping("managerBankRank")
	public String managerBankRank(StatisticsVo vo) {
		CallResult<List<Map<String, Object>>> result = new CallResult<>();
		try {
			List<Map<String, Object>> list = statisticsService.managerBankRank(vo);
			Integer count = statisticsService.managerBankPageCount(vo);
			result.setCount(count);
			result.setCode(0);
			result.setMessage("获取成功");
			result.setData(list);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			result.setCode(400);
			result.setMessage("获取失败");
			e.printStackTrace();
		}
		return gson1.toJson(result);
	}

	/**
	 * 获取银行区域
	 * 
	 * @param bb
	 * @return
	 */
	@PostMapping("getBankPage")
	public String getBankPage(BankBean bb) {
		Map<String, Object> map = new HashMap<>();
		try {
			map = bankBeanDaoService.getPagingData(bb);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			map.put("code", 400);
			map.put("message", "获取失败");
			e.printStackTrace();
		}
		return gson1.toJson(map);
	}

	/**
	 * 城市授信金额排名
	 * 
	 * @return
	 */
	@PostMapping("creditCtiyRank")
	public String creditCtiyRank(StatisticsVo vo) {
		CallResult<List<Map<String, Object>>> result = new CallResult<>();
		try {
			List<Map<String, Object>> list = statisticsService.creditCtiyRank(vo);
			Integer count = statisticsService.creditCtiyPageCount();
			result.setCount(count);
			result.setCode(0);
			result.setMessage("获取成功");
			result.setData(list);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			result.setCode(400);
			result.setMessage("获取失败");
			e.printStackTrace();
		}
		return gson1.toJson(result);
	}

	/**
	 * 银行授信金额排名
	 * 
	 * @return
	 */
	@PostMapping("creditBankRank")
	public String creditBankRank(StatisticsVo vo) {
		CallResult<List<Map<String, Object>>> result = new CallResult<>();
		try {
			List<Map<String, Object>> list = statisticsService.creditBankRank(vo);
			Integer count = statisticsService.creditBankPageCount();
			result.setCount(count);
			result.setCode(0);
			result.setMessage("获取成功");
			result.setData(list);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			result.setCode(400);
			result.setMessage("获取失败");
			e.printStackTrace();
		}
		return gson1.toJson(result);
	}
}
