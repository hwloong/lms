package com.hbsoft.lms.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hb.bean.CallResult;
import com.hb.controller.ABaseController;
import com.hbsoft.common.constant.Constant;
import com.hbsoft.lms.bean.LoanUseBean;
import com.hbsoft.lms.dao.LoanUseBeanDao;
import com.hbsoft.lms.dao.service.LoanUseBeanDaoService;
import com.hbsoft.util.GsonUtil;
import com.hbsoft.util.StringUtil;

@RestController
@RequestMapping(value = "", produces = "application/json;charset=UTF-8")
public class LoanUseController extends ABaseController {

	@Autowired
	LoanUseBeanDaoService loanUseBeanDaoService;
	@Autowired
	LoanUseBeanDao loanUseBeanDao;

	@PostMapping("addLoanUse")
	public String addLoanUse(LoanUseBean lub) {
		CallResult<String> result = new CallResult<>();
		try {
			LoanUseBean lub2 = new LoanUseBean();
			lub2.setName(lub.getName());
			LoanUseBean byField = loanUseBeanDaoService.getByField(lub2);
			if (byField == null) {
				String code = loanUseBeanDao.selectMaxCode();
				if (isEmpty(code)) {
					code = Constant.STARTCODE;
				} else {
					code = StringUtil.getCode(3, code);
				}
				lub.setId(code);
				lub.setCreateBy(String.valueOf(user_id_prikey));
				lub.setCreateOn(new Date());
				lub.setUpdateBy(String.valueOf(user_id_prikey));
				lub.setUpdateOn(new Date());
				loanUseBeanDaoService.add(lub);
				result.setCode(0);
				result.setMsg("操作成功");
			} else {
				result.setCode(400);
				result.setMsg("名称重复");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result.setCode(400);
			result.setMsg("操作失败");
		}
		return gson1.toJson(result);
	}

	@PostMapping("delLoanUse")
	public String delLoanUse(Integer id_prikey) {
		CallResult<String> result = new CallResult<>();
		try {
			if (id_prikey == null) {
				result.setCode(400);
				result.setMsg("主键参数有误");
			} else {
				loanUseBeanDaoService.removeOne(id_prikey);
				result.setCode(0);
				result.setMsg("操作成功");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result.setCode(400);
			result.setMsg("操作失败");
		}
		return gson1.toJson(result);
	}

	@PostMapping("updateLoanUse")
	public String updateLoanUse(LoanUseBean lub) {
		CallResult<String> result = new CallResult<>();
		if (lub.getId_prikey() == null) {
			result.setCode(400);
			result.setMsg("主键参数有误");
			return gson1.toJson(result);
		}
		if (isEmpty(lub.getName())) {
			result.setCode(400);
			result.setMsg("名称不能为空");
			return gson1.toJson(result);
		}
		try {
			lub.setUpdateBy(String.valueOf(user_id_prikey));
			lub.setUpdateOn(new Date());
			loanUseBeanDaoService.set(lub);
			result.setCode(0);
			result.setMsg("操作成功");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result.setCode(400);
			result.setMsg("操作失败");
		}
		return gson1.toJson(result);
	}
	
	@PostMapping("getLoanUseListPage")
	public String getLoanUseListPage(LoanUseBean lub) {
		Map<String,Object> map = new HashMap<>();
		try {
			map = loanUseBeanDaoService.getPagingData(lub);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			map.put("code", 400);
			map.put("msg", "获取失败");
			e.printStackTrace();
		}
		return gson1.toJson(map);
	}
	
	@PostMapping("getLoanUseById")
	public String getLoanUseById(Integer id_prikey) {
		CallResult<LoanUseBean> result = new CallResult<>();
		if(id_prikey==null){
			result.setCode(400);
			result.setMsg("主键参数有误");
			return gson1.toJson(result);
		}
		try {
			LoanUseBean byId = loanUseBeanDaoService.getById(id_prikey);
			result.setCode(0);
			result.setData(byId);
			result.setMsg("获取成功");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result.setCode(400);
			result.setMsg("操作失败");
		}
		return gson1.toJson(result);
	}
	
}
