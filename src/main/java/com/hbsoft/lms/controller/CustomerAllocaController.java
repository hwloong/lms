package com.hbsoft.lms.controller;

import com.hb.controller.ABaseController;
import com.hbsoft.lms.bean.CustomerAllocaBean;
import com.hbsoft.lms.dao.service.CustomerAllocaBeanDaoService;
import com.hbsoft.util.V1Pass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "", produces = "application/json;charset=UTF-8")
public class CustomerAllocaController extends ABaseController {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    CustomerAllocaBeanDaoService customerAllocaBeanDaoService;

    /**
     * 展示客户分配信息
     * @param customerAllocaBean
     * @return
     */
    @PostMapping("getCustomerAllocaInfo")
    public String getCustomerAllocaInfo(CustomerAllocaBean customerAllocaBean) {
        Map<String, Object> map = null;
        logger.info("获取客户分配信息开始");
        try {
            map = customerAllocaBeanDaoService.getCustomerAllocaInfo(customerAllocaBean);
            if (null != map && null != map.get("data")) {
                List<CustomerAllocaBean> list = (List<CustomerAllocaBean>)map.get("data");
                for (int i=0; i<list.size(); i++) {
                    list.get(i).setName(V1Pass.pass_decode(list.get(i).getName()));
                }
                return gson.toJson(map);
            }else {
                map = new HashMap<>();
                map.put("code",400);
                map .put("msg","暂无数据");
            }
        }catch (Exception e) {
            map = new HashMap<>();
            map.put("code",400);
            map .put("msg","暂无数据");
            e.printStackTrace();
        }
        logger.info("获取客户分配信息结束");
        return gson.toJson(map);
    }
}
