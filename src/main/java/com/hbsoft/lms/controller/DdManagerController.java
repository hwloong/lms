package com.hbsoft.lms.controller;

import java.util.*;

import com.hbsoft.common.enumtype.MethodEnum;
import com.hbsoft.config.SysConfigInfo;
import com.hbsoft.lms.bean.*;
import com.hbsoft.lms.dao.service.*;
import com.hbsoft.lms.service.BankDingService;
import com.hbsoft.lms.vo.DepartmentVo;
import com.hbsoft.util.StringUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hb.bean.CallResult;
import com.hb.controller.ABaseController;
import com.hbsoft.common.enumtype.AreaUserRoleEnum;
import com.hbsoft.lms.dao.DdTreeDao;
import com.hbsoft.util.DDUtil;
import com.hbsoft.util.GsonUtil;

@RestController
@RequestMapping(value = "", produces = "application/json;charset=UTF-8")
public class DdManagerController extends ABaseController {

	@Autowired
	private DdDeptBeanDaoService ddDeptBeanDaoService;
	@Autowired
	private DdUserBeanDaoService ddUserBeanDaoService;
	@Autowired
	private BankDingTokenBeanDaoService bankDingTokenBeanDaoService;
	@Autowired
	private DdTreeDao ddTreeDao;
	@Autowired
	private AreaUserBeanDaoService areaUserBeanDaoService;
	@Autowired
	private BankBeanDaoService bankBeanDaoService;
	@Autowired
	private DdDeptAllBeanDaoService ddDeptAllBeanDaoService;
	@Autowired
	private SysConfigInfo sysConfigInfo;

	@Autowired
	DDUtil ddUtil;

	@RequestMapping("getDdTree")
	public String getDdTree() {
		CallResult<List<DdDeptBean>> result = new CallResult<>();
		try {
			DdDeptBean param = new DdDeptBean();
			// param.setFlag("1");
			List<DdDeptBean> ddDeptList = ddDeptBeanDaoService.getAll(param);
			result.setCode(0);
			result.setMsg("操作成功");
			result.setData(ddDeptList);
		} catch (Exception e) {
			e.printStackTrace();
			result.setCode(0);
			result.setMsg("操作失败");
		}
		return GsonUtil.toJson(result);
	}

	/**
	 * 获取钉钉部门树状图
	 * 
	 * @return
	 */
	@PostMapping("getDdDeptTree")
	public String getDdDeptTree() {
		CallResult<List<Map<String, Object>>> result = new CallResult<>();
		try {
			CallResult<String> bankCodeResult = getBankCodeByLogin(AreaUserRoleEnum.AREA_USER_ROLE_Bank.getCode());
			if (bankCodeResult.getCode() == 0) {
				String bankCode = bankCodeResult.getData();
				List<Map<String, Object>> deptList = ddTreeDao.getDdDeptTree(bankCode);
				result.setCode(0);
				result.setMessage("获取成功");
				result.setData(deptList);
			} else {
				return gson1.toJson(bankCodeResult);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			result.setCode(400);
			result.setMessage("获取失败");
			e.printStackTrace();
		}
		return gson1.toJson(result);
	}

	/**
	 * 获取钉钉人员列表
	 * 
	 * @param
	 * @return
	 */
	@PostMapping("getDdUserTree")
	public String getDdUserTree(DdUserBean db) {
		Map<String, Object> map = new HashMap<>();
		try {
			CallResult<String> bankCodeResult = getBankCodeByLogin(AreaUserRoleEnum.AREA_USER_ROLE_Bank.getCode());
			if (bankCodeResult.getCode() == 0) {
				String bankCode = bankCodeResult.getData();
				db.setBankCode(bankCode);
				if (db.getDeptId() == null || "".equals(db.getDeptId())) {
					map = ddUserBeanDaoService.getPagingData(db);
				} else {
					db.setDeptId(db.getDeptId());
					map = ddUserBeanDaoService.getPagingData(db);
				}
                List<DdUserBean> list = (List<DdUserBean>) map.get("data");
				for(DdUserBean dduser : list){
				    dduser.setMobile(StringUtil.hidePhone(dduser.getMobile()));
                }
			}else {
				return gson1.toJson(bankCodeResult);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			map.put("code", 400);
			map.put("message", "操作失败");
			e.printStackTrace();
		}
		return gson1.toJson(map);
	}
	
	/**
	 * 添加和修改钉钉信息
	 * 
	 * @param bdt
	 * @return
	 */
	@PostMapping("addUpdateDingToken")
	public String addUpdateDingToken(BankDingTokenBean bdt) {
		CallResult<String> result = new CallResult<>();
		try {
			BankDingTokenBean corpID = new BankDingTokenBean();
			BankDingTokenBean serviceURL = new BankDingTokenBean();
			corpID.setCorpID(bdt.getCorpID());
			serviceURL.setServiceURL(bdt.getServiceURL());
			if (bdt.getId_prikey() == null || "".equals(bdt.getId_prikey())) {

//				BankDingTokenBean byServiceURL = bankDingTokenBeanDaoService.getByField(serviceURL);
				if(!sysConfigInfo.getCorpID().equals(bdt.getCorpID())) {
					BankDingTokenBean byCorpId = bankDingTokenBeanDaoService.getByField(corpID);
					if (byCorpId != null) {
						result.setCode(400);
						result.setMessage("钉钉企业id重复");
						return gson1.toJson(result);
					}
				}
//				if (byServiceURL != null) {
//					result.setCode(400);
//					result.setMessage("接口地址重复");
//					return gson1.toJson(result);
//				}

				CallResult<String> bankCodeResult = getBankCodeByLogin(AreaUserRoleEnum.AREA_USER_ROLE_Bank.getCode());
				if (bankCodeResult.getCode() != 0) {
					return gson1.toJson(bankCodeResult);
				}
				bdt.setCreateBy(String.valueOf(user_id_prikey));
				bdt.setCreateOn(new Date());
				bdt.setUpdateBy(String.valueOf(user_id_prikey));
				bdt.setUpdateOn(new Date());
				bdt.setBankCode(bankCodeResult.getData());
				bankDingTokenBeanDaoService.add(bdt);
			} else {
				BankDingTokenBean byId = bankDingTokenBeanDaoService.getById(bdt.getId_prikey());
				if (byId == null) {
					result.setCode(400);
					result.setMessage("id_prikey有误");
					return gson1.toJson(result);
				}
				if(!sysConfigInfo.getCorpID().equals(bdt.getCorpID())) {
					if (!byId.getCorpID().equals(bdt.getCorpID())) {
						BankDingTokenBean byCorpId = bankDingTokenBeanDaoService.getByField(corpID);
						if (byCorpId != null) {
							result.setCode(400);
							result.setMessage("钉钉企业id重复");
							return gson1.toJson(result);
						}
					}
				}
//				if (!byId.getServiceURL().equals(bdt.getServiceURL())){
//					BankDingTokenBean byServiceURL = bankDingTokenBeanDaoService.getByField(serviceURL);
//					if (byServiceURL != null) {
//						result.setCode(400);
//						result.setMessage("接口地址重复");
//						return gson1.toJson(result);
//					}
//				}
				bdt.setUpdateBy(String.valueOf(user_id_prikey));
				bdt.setUpdateOn(new Date());
				bankDingTokenBeanDaoService.set(bdt);
			}
			result.setCode(0);
			result.setMessage("操作成功");

		} catch (Exception e) {
			result.setCode(400);
			result.setMsg("操作失败");
			e.printStackTrace();
		}
		return gson1.toJson(result);
	}

	// @PostMapping("addBankDingToken")
	// public String addBankDingToken(BankDingTokenBean bdt) {
	// CallResult<String> result = new CallResult<>();
	// try {
	// bdt.setCreateBy(String.valueOf(user_id_prikey));
	// bdt.setCreateOn(new Date());
	// bdt.setUpdateBy(String.valueOf(user_id_prikey));
	// bdt.setUpdateOn(new Date());
	// bankDingTokenBeanDaoService.add(bdt);
	// result.setCode(0);
	// result.setMessage("操作成功");
	// } catch (Exception e) {
	// // TODO Auto-generated catch block
	// result.setCode(400);
	// result.setMsg("操作失败");
	// e.printStackTrace();
	// }
	// return GsonUtil.toJson(result);
	// }

	@PostMapping("delBankDingTokenById")
	public String delBankDingTokenById(Integer id_prikey) {
		CallResult<String> result = new CallResult<>();
		if (null == id_prikey) {
			result.setCode(400);
			result.setMessage("主键不能为空");
			return GsonUtil.toJson(result);
		}
		try {
			bankDingTokenBeanDaoService.removeOne(id_prikey);
			result.setCode(0);
			result.setMessage("操作成功");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			result.setCode(400);
			result.setMsg("操作失败");
			e.printStackTrace();
		}
		return GsonUtil.toJson(result);
	}

	// @PostMapping("updateBankDingToken")
	// public String updateBankDingToken(BankDingTokenBean bdt) {
	// CallResult<String> result = new CallResult<>();
	// if (null == bdt.getId_prikey()) {
	// result.setCode(400);
	// result.setMessage("主键不能为空");
	// return GsonUtil.toJson(result);
	// }
	// try {
	// bdt.setUpdateBy(String.valueOf(user_id_prikey));
	// bdt.setUpdateOn(new Date());
	// bankDingTokenBeanDaoService.set(bdt);
	// result.setCode(0);
	// result.setMessage("操作成功");
	// } catch (Exception e) {
	// // TODO Auto-generated catch block
	// result.setCode(400);
	// result.setMsg("操作失败");
	// e.printStackTrace();
	// }
	// return GsonUtil.toJson(result);
	// }

	@PostMapping("getBankDingTokenList")
	public String getBankDingTokenList(BankDingTokenBean bdt) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = bankDingTokenBeanDaoService.getPagingData(bdt);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			map.put("code", 400);
			map.put("message", "操作失败");
			e.printStackTrace();
		}
		return GsonUtil.toJson(map);
	}

	@PostMapping("getBankDingTokenById")
	public String getBankDingTokenById(Integer id_prikey) {
		CallResult<BankDingTokenBean> result = new CallResult<>();
		if (null == id_prikey) {
			result.setCode(400);
			result.setMessage("主键不能为空");
			return GsonUtil.toJson(result);
		}
		try {
			BankDingTokenBean byId = bankDingTokenBeanDaoService.getById(id_prikey);
			result.setCode(0);
			result.setMessage("操作成功");
			result.setData(byId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			result.setCode(400);
			result.setMsg("操作失败");
			e.printStackTrace();
		}
		return GsonUtil.toJson(result);
	}

	@PostMapping("getBankDingTokenByLogin")
	public String getBankDingTokenByLogin() {
		CallResult<BankDingTokenBean> result = new CallResult<>();
		try {
			CallResult<String> bankCodeResult = getBankCodeByLogin(AreaUserRoleEnum.AREA_USER_ROLE_Bank.getCode());
			if (!bankCodeResult.isExec()) {
				return GsonUtil.toJson(bankCodeResult);
			}
			BankDingTokenBean param = new BankDingTokenBean();
			param.setBankCode(bankCodeResult.getData());
			BankDingTokenBean byId = bankDingTokenBeanDaoService.getByField(param);
			result.setCode(0);
			result.setMessage("操作成功");
			result.setData(byId);
		} catch (Exception e) {
			result.setCode(400);
			result.setMsg("操作失败");
			e.printStackTrace();
		}
		return GsonUtil.toJson(result);
	}

	public CallResult<String> getBankCodeByLogin(String roleType) throws Exception {
		CallResult<String> result = new CallResult<>();
		AreaUserBean param = new AreaUserBean();
		param.setRoleType(roleType);
		param.setTel(hb_user.getName());
		AreaUserBean au = areaUserBeanDaoService.getByField(param);
		if (au == null || au.getCode() == null || "".equals(au.getCode())) {
			result.setCode(400);
			result.setMsg("权限不足");
			return result;
		} else {
			BankBean bbparam = new BankBean();
			bbparam.setCode(au.getCode());
			BankBean bankBean = bankBeanDaoService.getByField(bbparam);
			if (bankBean != null) {
				result.setData(bankBean.getBankCode());
			}
			result.setCode(0);
			result.setMsg("操作成功");
		}
		return result;
	}

	@PostMapping("getDdDeptAll")
	public String getDdDeptAll()  {
		CallResult<List<DdDeptAllBean>> result = new CallResult<>();
		try {
			List<DdDeptAllBean> all = ddDeptAllBeanDaoService.getAll(new DdDeptAllBean());
			result.setCode(0);
			result.setMsg("获取成功");
			result.setData(all);
		} catch (Exception e) {
			result.setCode(400);
			result.setMsg("操作失败");
			e.printStackTrace();
		}
		return gson.toJson(result);
	}

	@Scheduled(fixedDelay = 1000*60*60*5)
	public String setDdDeptAll(){
		CallResult<String> result = new CallResult<>();
		try {
			DepartmentVo depts = ddUtil.getDeptsByHbInterface(sysConfigInfo.getDdDeptInterfacePath(), MethodEnum.METHOD_ENUM_POST.getCode());
			if (depts.getDepartment()==null||depts.getDepartment().size()==0){
				result.setCode(400);
				result.setMsg("没有部门");
				return gson.toJson(result);
			}
			List<DdDeptBean> department = depts.getDepartment();

			ddDeptAllBeanDaoService.remove(null);
			for (DdDeptBean d : department) {
				DdDeptAllBean d2 = new DdDeptAllBean();
				BeanUtils.copyProperties(d,d2);
				d2.setPid(d2.getParentid());
				ddDeptAllBeanDaoService.add(d2);
			}
			result.setCode(0);
			result.setMsg("操作成功");
		} catch (Exception e) {
			result.setCode(400);
			result.setMsg("操作失败");
			e.printStackTrace();
		}
		return gson.toJson(result);
	}
}
