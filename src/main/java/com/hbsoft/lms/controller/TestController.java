package com.hbsoft.lms.controller;

import com.hb.annotation.LoginNotRequired;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

@Controller
public class TestController {


	private final Logger logger = LoggerFactory.getLogger(getClass());

	@ModelAttribute
	public void getClient(HttpSession session){


	}
	
	@LoginNotRequired
	@RequestMapping("test")
	@ResponseBody
	public String test() {
		return "SUCCESS";
	}

}
