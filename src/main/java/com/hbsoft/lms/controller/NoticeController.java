package com.hbsoft.lms.controller;

import com.hb.bean.CallResult;
import com.hb.controller.ABaseController;
import com.hbsoft.common.enumtype.NoticeTypeEnum;
import com.hbsoft.lms.bean.NoticeBean;
import com.hbsoft.lms.dao.service.NoticeBeanDaoService;
import com.hbsoft.lms.service.NoticeService;
import org.omg.CORBA.OBJ_ADAPTER;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "", produces = "application/json;charset=UTF-8")
public class NoticeController extends ABaseController {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    NoticeService noticeService;

    @Autowired
    NoticeBeanDaoService noticeBeanDaoService;

    /**
     * 获取发布公告
     * @return
     */
    @RequestMapping("getIssueNotice")
    public String issueNotice(NoticeBean noticeBean) {
        Map<String, Object> map = new HashMap<>();
        logger.info("获取公告开始");
        try {
            map = noticeService.getNotice(noticeBean);
        }catch (Exception e) {
            map.put("code",0);
            map.put("msg","获取失败");
            logger.error("获取公告失败" + e.getMessage(),e);
            e.printStackTrace();
        }
        logger.info("获取公告结束");
        return gson.toJson(map);
    }

    /**
     * 添加公告
     * @param noticeBean
     * @return
     */
    @RequestMapping("addNotice")
    public String addNotice(NoticeBean noticeBean) {
        CallResult<String> result = new CallResult<>();
        logger.info("添加公告");
        try {
            noticeBean.setIssId(user_id_prikey);
            noticeBean.setIssueTime(new Date());
            noticeBean.setState(Integer.valueOf(NoticeTypeEnum.NOTICE_TYPE_UNAUDIT.getCode()));
            noticeBeanDaoService.add(noticeBean);
            result.setCode(0);
            result.setMessage("添加成功");
        }catch (Exception e) {
            result.setCode(400);
            result.setMessage("添加失败");
            logger.error("添加公告失败" + e.getMessage(),e);
            e.printStackTrace();
        }
        logger.info("添加公告结束");
        return gson.toJson(result);
    }

    /**
     * 删除公告
     * @param noticeBean
     * @return
     */
    @RequestMapping("deleteNotice")
    public String deleteNotice(NoticeBean noticeBean) {
        CallResult<String> result = new CallResult<>();
        logger.info("删除公告开始");
        try {
            if (null == noticeBean.getId_prikey()) {
                result.setCode(400);
                result.setMessage("主键不能为空");
                return gson.toJson(result);
            }
            noticeBeanDaoService.removeOne(noticeBean.getId_prikey());
            result.setCode(0);
            result.setMessage("删除成功");
        }catch (Exception e) {
            result.setCode(400);
            result.setMessage("删除失败");
            logger.error("删除公告失败" + e.getMessage(),e);
            e.printStackTrace();
        }
        logger.info("删除公告结束");
        return gson.toJson(result);
    }

    /**
     * 获取单条公告详情
     * @param noticeBean
     * @return
     */
    @RequestMapping("getIssueNoticeById")
    public String getIssueNoticeById(NoticeBean noticeBean) {
        CallResult<String> result = new CallResult<>();
        logger.info("获取公告详情开始");
        try {
            if (null == noticeBean.getId_prikey()) {
                result.setCode(400);
                result.setMessage("主键不能为空");
                return gson.toJson(result);
            }
            noticeBeanDaoService.getById(noticeBean.getId_prikey());
            result.setCode(0);
            result.setMessage("获取成功");
        }catch (Exception e) {
            result.setCode(400);
            result.setMessage("获取公告详情失败");
            logger.error("获取公告详情失败" + e.getMessage(),e);
            e.printStackTrace();
        }
        logger.info("获取公告详情结束");
        return gson.toJson(result);
    }
    /**
     * 编辑公告
     * @param noticeBean
     * @return
     */
    @RequestMapping("updateNotice")
    public String updateNotice(NoticeBean noticeBean) {
        CallResult<String> result = new CallResult<>();
        logger.info("编辑公告开始");
        try {
            if (null == noticeBean.getId_prikey()) {
                result.setCode(400);
                result.setMessage("主键不能为空");
                return gson.toJson(result);
            }
            noticeBean.setIssId(user_id_prikey);
            noticeBean.setIssueTime(new Date());
            noticeBean.setAuditId(null);
            noticeBean.setAutiTime(null);
            noticeBean.setState(Integer.valueOf(NoticeTypeEnum.NOTICE_TYPE_UNAUDIT.getCode()));
            noticeBeanDaoService.set(noticeBean);
            result.setCode(0);
            result.setMessage("编辑成功");
        }catch (Exception e) {
            result.setCode(400);
            result.setMessage("编辑失败");
            e.printStackTrace();
        }
        logger.info("编辑公告结束");
        return gson.toJson(result);
    }

    /**
     * 获取公告审核列表
     * @return
     */
    @PostMapping("getNoticeExam")
    public String getNoticeExam(NoticeBean noticeBean) {
        Map<String, Object> map = null;
        logger.info("获取公告审核列表开始");
        try {
            map = noticeService.getNoticeExam(noticeBean);
            if (null != map && null != map.get("data")) {
                return gson.toJson(map);
            }else {
                map = new HashMap<>();
                map.put("code",400);
                map.put("msg","暂无数据");
            }
        }catch (Exception e) {
            map = new HashMap<>();
            map.put("code",400);
            map.put("msg","获取失败");
            e.printStackTrace();
        }
        logger.info("获取公告审核列表结束");
        return gson.toJson(map);
    }

    /**
     * 公告审核
     * @param noticeBean
     * @return
     */
    @RequestMapping("examNotice")
    public String examNotice(NoticeBean noticeBean) {
        CallResult<String> result = new CallResult<>();
        logger.info("公告审核开始");
        try {
            if (null == noticeBean.getId_prikey()) {
                result.setCode(400);
                result.setMessage("主键不能为空");
                return gson.toJson(result);
            }
            if (null == noticeBean.getState()) {
                result.setCode(400);
                result.setMessage("审核状态不能为空");
                return gson.toJson(result);
            }
            NoticeBean noticeBean1 = noticeBeanDaoService.getById(noticeBean.getId_prikey());
            if (null != noticeBean1.getAuditId() && noticeBean.getState() == 1) {
                result.setCode(400);
                result.setMessage("已审核");
                return gson.toJson(result);
            }
            if (noticeBean.getState() == 1) {
                noticeBean.setAuditId(user_id_prikey);
                noticeBean.setAutiTime(new Date());
                noticeBeanDaoService.set(noticeBean);
                result.setCode(0);
                result.setMessage("已通过");
            }else if(noticeBean.getState() == 2){
                noticeBean.setAuditId(user_id_prikey);
                noticeBean.setAutiTime(new Date());
                noticeBeanDaoService.set(noticeBean);
                result.setCode(0);
                result.setMessage("已驳回");
            }

        }catch (Exception e) {
            result.setCode(400);
            result.setMessage("审核失败");
            e.printStackTrace();
        }
        logger.info("公告审核结束");
        return gson.toJson(result);
    }
}
