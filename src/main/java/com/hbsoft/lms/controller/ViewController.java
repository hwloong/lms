package com.hbsoft.lms.controller;

import com.hb.bean.CallResult;
import com.hb.controller.ABaseController;
import com.hbsoft.common.enumtype.AreaUserRoleEnum;
import com.hbsoft.lms.bean.AreaUserBean;
import com.hbsoft.lms.bean.BankAreaBean;
import com.hbsoft.lms.bean.BankBean;
import com.hbsoft.lms.bean.BankBranchBean;
import com.hbsoft.lms.dao.service.AreaUserBeanDaoService;
import com.hbsoft.lms.dao.service.BankAreaBeanDaoService;
import com.hbsoft.lms.dao.service.BankBeanDaoService;
import com.hbsoft.lms.service.CustomerManagerService;
import com.hbsoft.lms.vo.DictVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class ViewController extends ABaseController {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final String  url = "";

    @Autowired
    private CustomerManagerService customerManagerService;

    @Autowired
    private AreaUserBeanDaoService areaUserBeanDaoService;

    @Autowired
    private BankAreaBeanDaoService bankAreaBeanDaoService;

    @Autowired
    private BankBeanDaoService bankBeanDaoService;

    @RequestMapping("bankArea")
    public String bankArea() {
        return url + "bankArea";
    }

    @RequestMapping("bankAreaAddAdminInfo")
    public String bankAreaAddAdminInfo() {
        return url + "bankAreaAddAdminInfo";
    }


    @RequestMapping("bankAreaAdminInfo")
    public String bankAreaAdminInfo() {
        return url + "bankAreaAdminInfo";
    }

    @RequestMapping("bankAddAdminInfo")
    public String bankAddAdminInfo() {
        return url + "bankAddAdminInfo";
    }

    @RequestMapping("bank")
    public String bank(Model model) {
        return url + "bank";
    }

    @RequestMapping("bankSimpleInfo")
    public String bankSimpleInfo(Model model) {
        CallResult<Map<String,Object>> result = getAreaCodeByLogin(AreaUserRoleEnum.AREA_USER_ROLE_SHIB.getCode());
        if(result.isExec()){
            model.addAttribute("areaName",result.getData().get("areaName"));
        }
        return url + "bankSimpleInfo";
    }

    @RequestMapping("bankAdminInfo")
    public String bankAdminInfo(Model model) {
        CallResult<Map<String,Object>> result = getAreaCodeByLogin(AreaUserRoleEnum.AREA_USER_ROLE_SHIB.getCode());
        if(result.isExec()){
            model.addAttribute("areaName",result.getData().get("areaName"));
        }
        return url + "bankAdminInfo";
    }

    @RequestMapping("bankBranch")
    public String bankBranch(Model model) {
       CallResult<Map<String,Object>> result = getAreaCodeByLogin(AreaUserRoleEnum.AREA_USER_ROLE_Bank.getCode());
        if(result.isExec()){
            Map<String,Object> map = result.getData();
            model.addAttribute("areaName",result.getData().get("areaName"));
        }
        return url + "bankBranch";
    }

    @RequestMapping("accountManagerInfo")
    public String accountManagerInfo(Model model) {
        try {
            CallResult<Map<String,Object>> result = getAreaCodeByLogin(AreaUserRoleEnum.AREA_USER_ROLE_Bank.getCode());
            BankBranchBean bbparam = new BankBranchBean();
            if(result.isExec()){
                model.addAttribute("areaName",result.getData().get("areaName"));
                bbparam.setAreaCode((String) result.getData().get("areaCode"));
            }
            List<DictVo> userTypeList = customerManagerService.getAllUserType();
            List<DictVo> userTeamList = customerManagerService.getAllBankBranchBean(bbparam);
            List<DictVo> loanUseList = customerManagerService.getAllLoanUse(null);
            model.addAttribute("userTypeList",userTypeList);
            model.addAttribute("userTeamList",userTeamList);
            model.addAttribute("loanUseList",loanUseList);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return url + "accountManagerInfo";
    }

    @RequestMapping("holidayInfo")
    public String holidayInfo() {
        return url + "holidayInfo";
    }

    @RequestMapping("customerInfo")
    public String customerInfo() {
        return url + "customerInfo";
    }

    @RequestMapping("customerEvaluateInfo")
    public String customerEvaluateInfo() {
        return url + "customerEvaluateInfo";
    }

    @RequestMapping("issueNotice")
    public String issueNotice() {
        return url + "issueNotice";
    }

    @RequestMapping("examNoticeView")
    public String examNoticeView() {
        return url + "examNoticeView";
    }

    @RequestMapping("customerAllocaInfo")
    public String customerAllocaInfo() {
        return url + "customerAllocaInfo";
    }

    @RequestMapping("loanUseInfo")
    public String loanUseInfo() {
    	return "loanUseInfo";
    }
    
    @RequestMapping("evaluateInfo")
    public String evaluateInfo() {
    	return "evaluateInfo";
    }
    
    @RequestMapping("ddDeptUserTreeView")
    public String ddDeptUserTreeView() {
    	return "ddDeptUserTreeView";
    }

    @RequestMapping("areaBankAuthInfo")
    public String areaBankAuthInfo() {
        return url + "areaBankAuthInfo";
    }
    
    @RequestMapping("bankDingTokenInfo")
    public String bankDingTokenInfo(Model model) {
        CallResult<Map<String,Object>> result = getAreaCodeByLogin(AreaUserRoleEnum.AREA_USER_ROLE_Bank.getCode());
        if(result.isExec()){
            model.addAttribute("areaName",result.getData().get("areaName"));
        }
        return "bankDingTokenInfo";
    }

    /**
     * 银行二维码
     * @return
     */
    @RequestMapping("bankQrCodeInfo")
    public String bankQrCodeInfo(Model model) {
        CallResult<Map<String,Object>> result = getAreaCodeByLogin(AreaUserRoleEnum.AREA_USER_ROLE_Bank.getCode());
        if(result.isExec()){
            model.addAttribute("areaName",result.getData().get("areaName"));
        }
        return "bankQrCodeInfo";
    }

    /**
     * 网点二维码
     * @return
     */
    @RequestMapping("bankBranchQrCodeInfo")
    public String bankBranchQrCodeInfo(Model model) {
        CallResult<Map<String,Object>> result = getAreaCodeByLogin(AreaUserRoleEnum.AREA_USER_ROLE_Bank.getCode());
        if(result.isExec()){
            model.addAttribute("areaName",result.getData().get("areaName"));
        }
        return "bankBranchQrCodeInfo";
    }

    /**
     * 客户经理二维码
     * @return
     */
    @RequestMapping("accountManagerQrCodeInfo")
    public String accountManagerQrCodeInfo() {return "accountManagerQrCodeInfo"; }

    public CallResult<Map<String,Object>> getAreaCodeByLogin(String roleType) {
        CallResult<Map<String,Object>> result = new CallResult<>();
        try {
            AreaUserBean param = new AreaUserBean();
            param.setRoleType(roleType);
            param.setTel(hb_user.getName());
            AreaUserBean au = areaUserBeanDaoService.getByField(param);
            if(au != null){
                Map<String,Object> map = new HashMap<>();
                String areaName = "";
                if(AreaUserRoleEnum.AREA_USER_ROLE_SHIB.getCode().equals(roleType)){
                    BankAreaBean baParam = new BankAreaBean();
                    baParam.setCode(au.getCode());
                    BankAreaBean bankAreaBean = bankAreaBeanDaoService.getByField(baParam);
                    if(bankAreaBean != null){
                        areaName = bankAreaBean.getName();
                    }
                }else if(AreaUserRoleEnum.AREA_USER_ROLE_Bank.getCode().equals(roleType)){
                    BankBean bkParam = new BankBean();
                    bkParam.setCode(au.getCode());
                    BankBean bankBean = bankBeanDaoService.getByField(bkParam);
                    if(bankBean != null){
                        areaName = bankBean.getName();
                    }
                }
                map.put("areaName",areaName);
                map.put("areaCode",au.getCode());
                result.setData(map);
                result.setCode(0);
                result.setMsg("操作成功");
            }else{
                result.setCode(400);
                result.setMsg("权限不足");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
