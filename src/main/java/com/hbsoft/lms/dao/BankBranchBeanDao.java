package com.hbsoft.lms.dao;

import org.apache.ibatis.annotations.Mapper;
import com.hb.dao.INewDao;
import com.hbsoft.lms.bean.BankBranchBean;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface BankBranchBeanDao extends INewDao<BankBranchBean,Integer> {

    String selectMaxCode(@Param("areaCode") String areaCode);
}
