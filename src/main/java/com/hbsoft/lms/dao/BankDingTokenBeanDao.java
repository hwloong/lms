package com.hbsoft.lms.dao;

import org.apache.ibatis.annotations.Mapper;
import com.hb.dao.INewDao;
import com.hbsoft.lms.bean.BankDingTokenBean;

@Mapper
public interface BankDingTokenBeanDao extends INewDao<BankDingTokenBean,Integer> {
}
