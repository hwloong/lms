package com.hbsoft.lms.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.hbsoft.lms.vo.StatisticsVo;

@Mapper
public interface StatisticsDao {

	String getAreaCodeByName(@Param("name") String name);
	
	Integer bespeakCount(StatisticsVo vo);
	
	Map<String,Object> bespeakFlow(StatisticsVo vo);
	
	Integer bankAreaCount();
	
	Integer bankCount();
	
	Integer managerCount();
	
	List<Map<String,Object>> managerBankRank(StatisticsVo vo);
	Integer managerBankPageCount(StatisticsVo vo);

	Integer administratorCount(@Param("len")String len,@Param("areaCode")String areaCode);

	List<Map<String,Object>> bespeakCityRank();	
	
	List<Map<String,Object>> bespeakBankRank(StatisticsVo vo);
	
	List<Map<String,Object>> bespeakFlowCityRank();

	List<Map<String, Object>> bespeakFlowBankRank(StatisticsVo vo);
	
	Integer creditCount();
	
	List<Map<String,Object>> creditCtiyRank(StatisticsVo vo);
	Integer creditCtiyPageCount();
	
	List<Map<String,Object>> creditBankRank(StatisticsVo vo);
	Integer creditBankPageCount();
	
}
