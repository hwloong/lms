package com.hbsoft.lms.dao;

import org.apache.ibatis.annotations.Mapper;
import com.hb.dao.INewDao;
import com.hbsoft.lms.bean.NoticeBean;

import java.util.List;

@Mapper
public interface NoticeBeanDao extends INewDao<NoticeBean,Integer> {
    List<NoticeBean> findNotice(NoticeBean noticeBean) throws Exception;

    List<NoticeBean> getNoticeExam(NoticeBean noticeBean) throws Exception;

    Integer getNoticeExamCount(NoticeBean noticeBean);
}
