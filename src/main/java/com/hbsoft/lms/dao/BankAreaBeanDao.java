package com.hbsoft.lms.dao;

import org.apache.ibatis.annotations.Mapper;
import com.hb.dao.INewDao;
import com.hbsoft.lms.bean.BankAreaBean;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface BankAreaBeanDao extends INewDao<BankAreaBean,Integer> {

    String selectMaxCode();

    List<BankAreaBean> getTreeByCode(@Param("param")String param);
}
