package com.hbsoft.lms.dao.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.hb.service.INewService;
import com.hb.exception.SqlNotChangeException;
import com.hbsoft.lms.dao.DdDeptAllBeanDao;
import com.hbsoft.lms.bean.DdDeptAllBean;

@Service
public class DdDeptAllBeanDaoService implements INewService<DdDeptAllBean,Integer> {
	@Autowired
	private DdDeptAllBeanDao ddDeptAllBeanDao;

	@Override
	public Boolean add(DdDeptAllBean ddDeptAllBean) throws Exception {
		Integer i = ddDeptAllBeanDao.insert(ddDeptAllBean);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return true;
	}

	@Override
	public Integer addPrikey(DdDeptAllBean ddDeptAllBean) throws Exception {
		Integer prikey = null;
		Integer i = ddDeptAllBeanDao.insertPrikey(ddDeptAllBean);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		Object o = ddDeptAllBean.getId_prikey();
		if (o instanceof Integer) {
			prikey = (Integer) o;
		}
		return prikey;
	}

	@Override
	public Integer addBatch(List<DdDeptAllBean> list) throws Exception {
		Integer i = ddDeptAllBeanDao.insertBatch(list);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return i;
	}

	@Override
	public Integer remove(DdDeptAllBean ddDeptAllBean) throws Exception {
		Integer i = ddDeptAllBeanDao.delete(ddDeptAllBean);
		return i;
	}

	@Override
	public Boolean removeOne(Integer id) throws Exception {
		Integer i = ddDeptAllBeanDao.deleteOne(id);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return true;
	}

	@Override
	public Integer removeBatch(List<Integer> ids) throws Exception {
		Integer i = ddDeptAllBeanDao.deleteBatch(ids);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return i;
	}

	@Override
	public Boolean set(DdDeptAllBean ddDeptAllBean) throws Exception {
		Integer i = ddDeptAllBeanDao.update(ddDeptAllBean);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public Boolean setEmpty(DdDeptAllBean ddDeptAllBean) throws Exception {
		Integer i = ddDeptAllBeanDao.updateEmpty(ddDeptAllBean);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public DdDeptAllBean getById(Integer id) throws Exception {
		return ddDeptAllBeanDao.findById(id);
	}

	@Override
	public DdDeptAllBean getByField(DdDeptAllBean ddDeptAllBean) throws Exception {
		return ddDeptAllBeanDao.findByField(ddDeptAllBean);
	}

	@Override
	public List<DdDeptAllBean> getAll(DdDeptAllBean ddDeptAllBean) throws Exception {
		return ddDeptAllBeanDao.findAll(ddDeptAllBean);
	}

	@Override
	public Map<String, Object> getPagingData(DdDeptAllBean ddDeptAllBean) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		Integer count = ddDeptAllBeanDao.findPagingCount(ddDeptAllBean);
		List<DdDeptAllBean> list = ddDeptAllBeanDao.findPagingData(ddDeptAllBean);
		map.put("code",0);
		map.put("msg","获取成功");
		map.put("count", count);
		map.put("data", list);
		return map;
	}
	
	@Override
	public List<DdDeptAllBean> getTreeAll(DdDeptAllBean ddDeptAllBean) throws Exception {
		return ddDeptAllBeanDao.findTreeData(ddDeptAllBean);
	}
}
