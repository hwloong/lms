package com.hbsoft.lms.dao.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.hb.service.INewService;
import com.hb.exception.SqlNotChangeException;
import com.hbsoft.lms.dao.BankAreaBeanDao;
import com.hbsoft.lms.bean.BankAreaBean;

@Service
public class BankAreaBeanDaoService implements INewService<BankAreaBean,Integer> {
	@Autowired
	private BankAreaBeanDao bankAreaBeanDao;

	@Override
	public Boolean add(BankAreaBean bankAreaBean) throws Exception {
		Integer i = bankAreaBeanDao.insert(bankAreaBean);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return true;
	}

	@Override
	public Integer addPrikey(BankAreaBean bankAreaBean) throws Exception {
		Integer prikey = null;
		Integer i = bankAreaBeanDao.insertPrikey(bankAreaBean);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		Object o = bankAreaBean.getId_prikey();
		if (o instanceof Integer) {
			prikey = (Integer) o;
		}
		return prikey;
	}

	@Override
	public Integer addBatch(List<BankAreaBean> list) throws Exception {
		Integer i = bankAreaBeanDao.insertBatch(list);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return i;
	}

	@Override
	public Integer remove(BankAreaBean bankAreaBean) throws Exception {
		Integer i = bankAreaBeanDao.delete(bankAreaBean);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return i;
	}

	@Override
	public Boolean removeOne(Integer id) throws Exception {
		Integer i = bankAreaBeanDao.deleteOne(id);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return true;
	}

	@Override
	public Integer removeBatch(List<Integer> ids) throws Exception {
		Integer i = bankAreaBeanDao.deleteBatch(ids);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return i;
	}

	@Override
	public Boolean set(BankAreaBean bankAreaBean) throws Exception {
		Integer i = bankAreaBeanDao.update(bankAreaBean);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public Boolean setEmpty(BankAreaBean bankAreaBean) throws Exception {
		Integer i = bankAreaBeanDao.updateEmpty(bankAreaBean);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public BankAreaBean getById(Integer id) throws Exception {
		return bankAreaBeanDao.findById(id);
	}

	@Override
	public BankAreaBean getByField(BankAreaBean bankAreaBean) throws Exception {
		return bankAreaBeanDao.findByField(bankAreaBean);
	}

	@Override
	public List<BankAreaBean> getAll(BankAreaBean bankAreaBean) throws Exception {
		return bankAreaBeanDao.findAll(bankAreaBean);
	}

	@Override
	public Map<String, Object> getPagingData(BankAreaBean bankAreaBean) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		Integer count = bankAreaBeanDao.findPagingCount(bankAreaBean);
		List<BankAreaBean> list = bankAreaBeanDao.findPagingData(bankAreaBean);
		map.put("code",0);
		map.put("msg","获取成功");
		map.put("count", count);
		map.put("data", list);
		return map;
	}
	
	@Override
	public List<BankAreaBean> getTreeAll(BankAreaBean bankAreaBean) throws Exception {
		return bankAreaBeanDao.findTreeData(bankAreaBean);
	}

    public List<BankAreaBean> getTreeByCode(String param) {
		return bankAreaBeanDao.getTreeByCode(param);
    }
}
