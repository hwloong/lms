package com.hbsoft.lms.dao.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.hb.service.INewService;
import com.hb.exception.SqlNotChangeException;
import com.hbsoft.lms.dao.HolidayBeanDao;
import com.hbsoft.lms.bean.HolidayBean;

@Service
public class HolidayBeanDaoService implements INewService<HolidayBean,Integer> {
	@Autowired
	private HolidayBeanDao holidayBeanDao;

	@Override
	public Boolean add(HolidayBean holidayBean) throws Exception {
		Integer i = holidayBeanDao.insert(holidayBean);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return true;
	}

	@Override
	public Integer addPrikey(HolidayBean holidayBean) throws Exception {
		Integer prikey = null;
		Integer i = holidayBeanDao.insertPrikey(holidayBean);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		Object o = holidayBean.getId_prikey();
		if (o instanceof Integer) {
			prikey = (Integer) o;
		}
		return prikey;
	}

	@Override
	public Integer addBatch(List<HolidayBean> list) throws Exception {
		Integer i = holidayBeanDao.insertBatch(list);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return i;
	}

	@Override
	public Integer remove(HolidayBean holidayBean) throws Exception {
		Integer i = holidayBeanDao.delete(holidayBean);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return i;
	}

	@Override
	public Boolean removeOne(Integer id) throws Exception {
		Integer i = holidayBeanDao.deleteOne(id);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return true;
	}

	@Override
	public Integer removeBatch(List<Integer> ids) throws Exception {
		Integer i = holidayBeanDao.deleteBatch(ids);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return i;
	}

	@Override
	public Boolean set(HolidayBean holidayBean) throws Exception {
		Integer i = holidayBeanDao.update(holidayBean);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public Boolean setEmpty(HolidayBean holidayBean) throws Exception {
		Integer i = holidayBeanDao.updateEmpty(holidayBean);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public HolidayBean getById(Integer id) throws Exception {
		return holidayBeanDao.findById(id);
	}

	@Override
	public HolidayBean getByField(HolidayBean holidayBean) throws Exception {
		return holidayBeanDao.findByField(holidayBean);
	}

	@Override
	public List<HolidayBean> getAll(HolidayBean holidayBean) throws Exception {
		return holidayBeanDao.findAll(holidayBean);
	}

	@Override
	public Map<String, Object> getPagingData(HolidayBean holidayBean) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		Integer count = holidayBeanDao.findPagingCount(holidayBean);
		List<HolidayBean> list = holidayBeanDao.findPagingData(holidayBean);
		map.put("code",0);
		map.put("msg","获取成功");
		map.put("count", count);
		map.put("data", list);
		return map;
	}
	
	@Override
	public List<HolidayBean> getTreeAll(HolidayBean holidayBean) throws Exception {
		return holidayBeanDao.findTreeData(holidayBean);
	}
}
