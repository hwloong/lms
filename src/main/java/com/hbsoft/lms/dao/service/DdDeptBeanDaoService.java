package com.hbsoft.lms.dao.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.hb.service.INewService;
import com.hb.exception.SqlNotChangeException;
import com.hbsoft.lms.dao.DdDeptBeanDao;
import com.hbsoft.lms.bean.DdDeptBean;

@Service
public class DdDeptBeanDaoService implements INewService<DdDeptBean,Integer> {
	@Autowired
	private DdDeptBeanDao ddDeptBeanDao;

	@Override
	public Boolean add(DdDeptBean ddDeptBean) throws Exception {
		Integer i = ddDeptBeanDao.insert(ddDeptBean);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return true;
	}

	@Override
	public Integer addPrikey(DdDeptBean ddDeptBean) throws Exception {
		Integer prikey = null;
		Integer i = ddDeptBeanDao.insertPrikey(ddDeptBean);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		Object o = ddDeptBean.getId_prikey();
		if (o instanceof Integer) {
			prikey = (Integer) o;
		}
		return prikey;
	}

	@Override
	public Integer addBatch(List<DdDeptBean> list) throws Exception {
		Integer i = ddDeptBeanDao.insertBatch(list);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return i;
	}

	@Override
	public Integer remove(DdDeptBean ddDeptBean) throws Exception {
		Integer i = ddDeptBeanDao.delete(ddDeptBean);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return i;
	}

	@Override
	public Boolean removeOne(Integer id) throws Exception {
		Integer i = ddDeptBeanDao.deleteOne(id);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return true;
	}

	@Override
	public Integer removeBatch(List<Integer> ids) throws Exception {
		Integer i = ddDeptBeanDao.deleteBatch(ids);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return i;
	}

	@Override
	public Boolean set(DdDeptBean ddDeptBean) throws Exception {
		Integer i = ddDeptBeanDao.update(ddDeptBean);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public Boolean setEmpty(DdDeptBean ddDeptBean) throws Exception {
		Integer i = ddDeptBeanDao.updateEmpty(ddDeptBean);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public DdDeptBean getById(Integer id) throws Exception {
		return ddDeptBeanDao.findById(id);
	}

	@Override
	public DdDeptBean getByField(DdDeptBean ddDeptBean) throws Exception {
		return ddDeptBeanDao.findByField(ddDeptBean);
	}

	@Override
	public List<DdDeptBean> getAll(DdDeptBean ddDeptBean) throws Exception {
		return ddDeptBeanDao.findAll(ddDeptBean);
	}

	@Override
	public Map<String, Object> getPagingData(DdDeptBean ddDeptBean) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		Integer count = ddDeptBeanDao.findPagingCount(ddDeptBean);
		List<DdDeptBean> list = ddDeptBeanDao.findPagingData(ddDeptBean);
		map.put("code",0);
		map.put("msg","获取成功");
		map.put("count", count);
		map.put("data", list);
		return map;
	}
	
	@Override
	public List<DdDeptBean> getTreeAll(DdDeptBean ddDeptBean) throws Exception {
		return ddDeptBeanDao.findTreeData(ddDeptBean);
	}
}
