package com.hbsoft.lms.dao.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.hb.service.INewService;
import com.hb.exception.SqlNotChangeException;
import com.hbsoft.lms.dao.QrCodeBeanDao;
import com.hbsoft.lms.bean.QrCodeBean;

@Service
public class QrCodeBeanDaoService implements INewService<QrCodeBean,Integer> {
	@Autowired
	private QrCodeBeanDao qrCodeBeanDao;

	@Override
	public Boolean add(QrCodeBean qrCodeBean) throws Exception {
		Integer i = qrCodeBeanDao.insert(qrCodeBean);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return true;
	}

	@Override
	public Integer addPrikey(QrCodeBean qrCodeBean) throws Exception {
		Integer prikey = null;
		Integer i = qrCodeBeanDao.insertPrikey(qrCodeBean);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		Object o = qrCodeBean.getId_prikey();
		if (o instanceof Integer) {
			prikey = (Integer) o;
		}
		return prikey;
	}

	@Override
	public Integer addBatch(List<QrCodeBean> list) throws Exception {
		Integer i = qrCodeBeanDao.insertBatch(list);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return i;
	}

	@Override
	public Integer remove(QrCodeBean qrCodeBean) throws Exception {
		Integer i = qrCodeBeanDao.delete(qrCodeBean);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return i;
	}

	@Override
	public Boolean removeOne(Integer id) throws Exception {
		Integer i = qrCodeBeanDao.deleteOne(id);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return true;
	}

	@Override
	public Integer removeBatch(List<Integer> ids) throws Exception {
		Integer i = qrCodeBeanDao.deleteBatch(ids);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return i;
	}

	@Override
	public Boolean set(QrCodeBean qrCodeBean) throws Exception {
		Integer i = qrCodeBeanDao.update(qrCodeBean);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public Boolean setEmpty(QrCodeBean qrCodeBean) throws Exception {
		Integer i = qrCodeBeanDao.updateEmpty(qrCodeBean);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public QrCodeBean getById(Integer id) throws Exception {
		return qrCodeBeanDao.findById(id);
	}

	@Override
	public QrCodeBean getByField(QrCodeBean qrCodeBean) throws Exception {
		return qrCodeBeanDao.findByField(qrCodeBean);
	}

	@Override
	public List<QrCodeBean> getAll(QrCodeBean qrCodeBean) throws Exception {
		return qrCodeBeanDao.findAll(qrCodeBean);
	}

	@Override
	public Map<String, Object> getPagingData(QrCodeBean qrCodeBean) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		Integer count = qrCodeBeanDao.findPagingCount(qrCodeBean);
		List<QrCodeBean> list = qrCodeBeanDao.findPagingData(qrCodeBean);
		map.put("code",0);
		map.put("msg","获取成功");
		map.put("count", count);
		map.put("data", list);
		return map;
	}
	
	@Override
	public List<QrCodeBean> getTreeAll(QrCodeBean qrCodeBean) throws Exception {
		return qrCodeBeanDao.findTreeData(qrCodeBean);
	}
}
