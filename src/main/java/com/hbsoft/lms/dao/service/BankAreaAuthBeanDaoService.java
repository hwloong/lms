package com.hbsoft.lms.dao.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.hb.service.INewService;
import com.hb.exception.SqlNotChangeException;
import com.hbsoft.lms.dao.BankAreaAuthBeanDao;
import com.hbsoft.lms.bean.BankAreaAuthBean;

@Service
public class BankAreaAuthBeanDaoService implements INewService<BankAreaAuthBean,Integer> {
	@Autowired
	private BankAreaAuthBeanDao bankAreaAuthBeanDao;

	@Override
	public Boolean add(BankAreaAuthBean bankAreaAuthBean) throws Exception {
		Integer i = bankAreaAuthBeanDao.insert(bankAreaAuthBean);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return true;
	}

	@Override
	public Integer addPrikey(BankAreaAuthBean bankAreaAuthBean) throws Exception {
		Integer prikey = null;
		Integer i = bankAreaAuthBeanDao.insertPrikey(bankAreaAuthBean);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		Object o = bankAreaAuthBean.getId_prikey();
		if (o instanceof Integer) {
			prikey = (Integer) o;
		}
		return prikey;
	}

	@Override
	public Integer addBatch(List<BankAreaAuthBean> list) throws Exception {
		Integer i = bankAreaAuthBeanDao.insertBatch(list);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return i;
	}

	@Override
	public Integer remove(BankAreaAuthBean bankAreaAuthBean) throws Exception {
		Integer i = bankAreaAuthBeanDao.delete(bankAreaAuthBean);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return i;
	}

	@Override
	public Boolean removeOne(Integer id) throws Exception {
		Integer i = bankAreaAuthBeanDao.deleteOne(id);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return true;
	}

	@Override
	public Integer removeBatch(List<Integer> ids) throws Exception {
		Integer i = bankAreaAuthBeanDao.deleteBatch(ids);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return i;
	}

	@Override
	public Boolean set(BankAreaAuthBean bankAreaAuthBean) throws Exception {
		Integer i = bankAreaAuthBeanDao.update(bankAreaAuthBean);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public Boolean setEmpty(BankAreaAuthBean bankAreaAuthBean) throws Exception {
		Integer i = bankAreaAuthBeanDao.updateEmpty(bankAreaAuthBean);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public BankAreaAuthBean getById(Integer id) throws Exception {
		return bankAreaAuthBeanDao.findById(id);
	}

	@Override
	public BankAreaAuthBean getByField(BankAreaAuthBean bankAreaAuthBean) throws Exception {
		return bankAreaAuthBeanDao.findByField(bankAreaAuthBean);
	}

	@Override
	public List<BankAreaAuthBean> getAll(BankAreaAuthBean bankAreaAuthBean) throws Exception {
		return bankAreaAuthBeanDao.findAll(bankAreaAuthBean);
	}

	@Override
	public Map<String, Object> getPagingData(BankAreaAuthBean bankAreaAuthBean) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		Integer count = bankAreaAuthBeanDao.findPagingCount(bankAreaAuthBean);
		List<BankAreaAuthBean> list = bankAreaAuthBeanDao.findPagingData(bankAreaAuthBean);
		map.put("code",0);
		map.put("msg","获取成功");
		map.put("count", count);
		map.put("data", list);
		return map;
	}
	
	@Override
	public List<BankAreaAuthBean> getTreeAll(BankAreaAuthBean bankAreaAuthBean) throws Exception {
		return bankAreaAuthBeanDao.findTreeData(bankAreaAuthBean);
	}

	public List<BankAreaAuthBean> getAuthList(String id) {
		return bankAreaAuthBeanDao.getAuthList(id);
	}
}
