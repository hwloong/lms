package com.hbsoft.lms.dao.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.hb.service.INewService;
import com.hb.exception.SqlNotChangeException;
import com.hbsoft.lms.dao.BankBeanDao;
import com.hbsoft.lms.bean.BankBean;

@Service
public class BankBeanDaoService implements INewService<BankBean,Integer> {
	@Autowired
	private BankBeanDao bankBeanDao;

	@Override
	public Boolean add(BankBean bankBean) throws Exception {
		Integer i = bankBeanDao.insert(bankBean);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return true;
	}

	@Override
	public Integer addPrikey(BankBean bankBean) throws Exception {
		Integer prikey = null;
		Integer i = bankBeanDao.insertPrikey(bankBean);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		Object o = bankBean.getId_prikey();
		if (o instanceof Integer) {
			prikey = (Integer) o;
		}
		return prikey;
	}

	@Override
	public Integer addBatch(List<BankBean> list) throws Exception {
		Integer i = bankBeanDao.insertBatch(list);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return i;
	}

	@Override
	public Integer remove(BankBean bankBean) throws Exception {
		Integer i = bankBeanDao.delete(bankBean);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return i;
	}

	@Override
	public Boolean removeOne(Integer id) throws Exception {
		Integer i = bankBeanDao.deleteOne(id);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return true;
	}

	@Override
	public Integer removeBatch(List<Integer> ids) throws Exception {
		Integer i = bankBeanDao.deleteBatch(ids);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return i;
	}

	@Override
	public Boolean set(BankBean bankBean) throws Exception {
		Integer i = bankBeanDao.update(bankBean);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public Boolean setEmpty(BankBean bankBean) throws Exception {
		Integer i = bankBeanDao.updateEmpty(bankBean);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public BankBean getById(Integer id) throws Exception {
		return bankBeanDao.findById(id);
	}

	@Override
	public BankBean getByField(BankBean bankBean) throws Exception {
		return bankBeanDao.findByField(bankBean);
	}

	@Override
	public List<BankBean> getAll(BankBean bankBean) throws Exception {
		return bankBeanDao.findAll(bankBean);
	}

	@Override
	public Map<String, Object> getPagingData(BankBean bankBean) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		Integer count = bankBeanDao.findPagingCount(bankBean);
		List<BankBean> list = bankBeanDao.findPagingData(bankBean);
		map.put("code",0);
		map.put("msg","获取成功");
		map.put("count", count);
		map.put("data", list);
		return map;
	}
	
	@Override
	public List<BankBean> getTreeAll(BankBean bankBean) throws Exception {
		return bankBeanDao.findTreeData(bankBean);
	}

    public List<BankBean> getTreeAllByCode(String param) {
		return bankBeanDao.getTreeAllByCode(param);
    }
}
