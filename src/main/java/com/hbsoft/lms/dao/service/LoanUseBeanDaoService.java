package com.hbsoft.lms.dao.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.hb.service.INewService;
import com.hb.exception.SqlNotChangeException;
import com.hbsoft.lms.dao.LoanUseBeanDao;
import com.hbsoft.lms.bean.LoanUseBean;

@Service
public class LoanUseBeanDaoService implements INewService<LoanUseBean,Integer> {
	@Autowired
	private LoanUseBeanDao loanUseBeanDao;

	@Override
	public Boolean add(LoanUseBean loanUseBean) throws Exception {
		Integer i = loanUseBeanDao.insert(loanUseBean);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return true;
	}

	@Override
	public Integer addPrikey(LoanUseBean loanUseBean) throws Exception {
		Integer prikey = null;
		Integer i = loanUseBeanDao.insertPrikey(loanUseBean);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		Object o = loanUseBean.getId_prikey();
		if (o instanceof Integer) {
			prikey = (Integer) o;
		}
		return prikey;
	}

	@Override
	public Integer addBatch(List<LoanUseBean> list) throws Exception {
		Integer i = loanUseBeanDao.insertBatch(list);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return i;
	}

	@Override
	public Integer remove(LoanUseBean loanUseBean) throws Exception {
		Integer i = loanUseBeanDao.delete(loanUseBean);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return i;
	}

	@Override
	public Boolean removeOne(Integer id) throws Exception {
		Integer i = loanUseBeanDao.deleteOne(id);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return true;
	}

	@Override
	public Integer removeBatch(List<Integer> ids) throws Exception {
		Integer i = loanUseBeanDao.deleteBatch(ids);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return i;
	}

	@Override
	public Boolean set(LoanUseBean loanUseBean) throws Exception {
		Integer i = loanUseBeanDao.update(loanUseBean);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public Boolean setEmpty(LoanUseBean loanUseBean) throws Exception {
		Integer i = loanUseBeanDao.updateEmpty(loanUseBean);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public LoanUseBean getById(Integer id) throws Exception {
		return loanUseBeanDao.findById(id);
	}

	@Override
	public LoanUseBean getByField(LoanUseBean loanUseBean) throws Exception {
		return loanUseBeanDao.findByField(loanUseBean);
	}

	@Override
	public List<LoanUseBean> getAll(LoanUseBean loanUseBean) throws Exception {
		return loanUseBeanDao.findAll(loanUseBean);
	}

	@Override
	public Map<String, Object> getPagingData(LoanUseBean loanUseBean) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		Integer count = loanUseBeanDao.findPagingCount(loanUseBean);
		List<LoanUseBean> list = loanUseBeanDao.findPagingData(loanUseBean);
		map.put("code",0);
		map.put("msg","获取成功");
		map.put("count", count);
		map.put("data", list);
		return map;
	}
	
	@Override
	public List<LoanUseBean> getTreeAll(LoanUseBean loanUseBean) throws Exception {
		return loanUseBeanDao.findTreeData(loanUseBean);
	}
}
