package com.hbsoft.lms.dao.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.hb.service.INewService;
import com.hb.exception.SqlNotChangeException;
import com.hbsoft.lms.dao.AreaUserBeanDao;
import com.hbsoft.lms.bean.AreaUserBean;

@Service
public class AreaUserBeanDaoService implements INewService<AreaUserBean,Integer> {
	@Autowired
	private AreaUserBeanDao areaUserBeanDao;

	@Override
	public Boolean add(AreaUserBean areaUserBean) throws Exception {
		Integer i = areaUserBeanDao.insert(areaUserBean);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return true;
	}

	@Override
	public Integer addPrikey(AreaUserBean areaUserBean) throws Exception {
		Integer prikey = null;
		Integer i = areaUserBeanDao.insertPrikey(areaUserBean);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		Object o = areaUserBean.getId_prikey();
		if (o instanceof Integer) {
			prikey = (Integer) o;
		}
		return prikey;
	}

	@Override
	public Integer addBatch(List<AreaUserBean> list) throws Exception {
		Integer i = areaUserBeanDao.insertBatch(list);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return i;
	}

	@Override
	public Integer remove(AreaUserBean areaUserBean) throws Exception {
		Integer i = areaUserBeanDao.delete(areaUserBean);
		if(i==0){
			//throw new SqlNotChangeException("删除失败");
		}
		return i;
	}

	@Override
	public Boolean removeOne(Integer id) throws Exception {
		Integer i = areaUserBeanDao.deleteOne(id);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return true;
	}

	@Override
	public Integer removeBatch(List<Integer> ids) throws Exception {
		Integer i = areaUserBeanDao.deleteBatch(ids);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return i;
	}

	@Override
	public Boolean set(AreaUserBean areaUserBean) throws Exception {
		Integer i = areaUserBeanDao.update(areaUserBean);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public Boolean setEmpty(AreaUserBean areaUserBean) throws Exception {
		Integer i = areaUserBeanDao.updateEmpty(areaUserBean);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public AreaUserBean getById(Integer id) throws Exception {
		return areaUserBeanDao.findById(id);
	}

	@Override
	public AreaUserBean getByField(AreaUserBean areaUserBean) throws Exception {
		return areaUserBeanDao.findByField(areaUserBean);
	}

	@Override
	public List<AreaUserBean> getAll(AreaUserBean areaUserBean) throws Exception {
		return areaUserBeanDao.findAll(areaUserBean);
	}

	@Override
	public Map<String, Object> getPagingData(AreaUserBean areaUserBean) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		Integer count = areaUserBeanDao.findPagingCount(areaUserBean);
		List<AreaUserBean> list = areaUserBeanDao.findPagingData(areaUserBean);
		map.put("code",0);
		map.put("msg","获取成功");
		map.put("count", count);
		map.put("data", list);
		return map;
	}

	public Map<String, Object> getAllPaginData(AreaUserBean areaUserBean) {
		Map<String, Object> map = new HashMap<String, Object>();
		Integer count = areaUserBeanDao.findAllPagingCount(areaUserBean);
		List<AreaUserBean> list = areaUserBeanDao.findAllPagingData(areaUserBean);
		map.put("code",0);
		map.put("msg","获取成功");
		map.put("count", count);
		map.put("data", list);
		return map;
	}

	
	@Override
	public List<AreaUserBean> getTreeAll(AreaUserBean areaUserBean) throws Exception {
		return areaUserBeanDao.findTreeData(areaUserBean);
	}


}
