package com.hbsoft.lms.dao.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.hb.service.INewService;
import com.hb.exception.SqlNotChangeException;
import com.hbsoft.lms.dao.CustomerAllocaBeanDao;
import com.hbsoft.lms.bean.CustomerAllocaBean;

@Service
public class CustomerAllocaBeanDaoService implements INewService<CustomerAllocaBean,Integer> {
	@Autowired
	private CustomerAllocaBeanDao customerAllocaBeanDao;

	@Override
	public Boolean add(CustomerAllocaBean customerAllocaBean) throws Exception {
		Integer i = customerAllocaBeanDao.insert(customerAllocaBean);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return true;
	}

	@Override
	public Integer addPrikey(CustomerAllocaBean customerAllocaBean) throws Exception {
		Integer prikey = null;
		Integer i = customerAllocaBeanDao.insertPrikey(customerAllocaBean);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		Object o = customerAllocaBean.getId();
		if (o instanceof Integer) {
			prikey = (Integer) o;
		}
		return prikey;
	}

	@Override
	public Integer addBatch(List<CustomerAllocaBean> list) throws Exception {
		Integer i = customerAllocaBeanDao.insertBatch(list);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return i;
	}

	@Override
	public Integer remove(CustomerAllocaBean customerAllocaBean) throws Exception {
		Integer i = customerAllocaBeanDao.delete(customerAllocaBean);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return i;
	}

	@Override
	public Boolean removeOne(Integer id) throws Exception {
		Integer i = customerAllocaBeanDao.deleteOne(id);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return true;
	}

	@Override
	public Integer removeBatch(List<Integer> ids) throws Exception {
		Integer i = customerAllocaBeanDao.deleteBatch(ids);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return i;
	}

	@Override
	public Boolean set(CustomerAllocaBean customerAllocaBean) throws Exception {
		Integer i = customerAllocaBeanDao.update(customerAllocaBean);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public Boolean setEmpty(CustomerAllocaBean customerAllocaBean) throws Exception {
		Integer i = customerAllocaBeanDao.updateEmpty(customerAllocaBean);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public CustomerAllocaBean getById(Integer id) throws Exception {
		return customerAllocaBeanDao.findById(id);
	}

	@Override
	public CustomerAllocaBean getByField(CustomerAllocaBean customerAllocaBean) throws Exception {
		return customerAllocaBeanDao.findByField(customerAllocaBean);
	}

	@Override
	public List<CustomerAllocaBean> getAll(CustomerAllocaBean customerAllocaBean) throws Exception {
		return customerAllocaBeanDao.findAll(customerAllocaBean);
	}

	@Override
	public Map<String, Object> getPagingData(CustomerAllocaBean customerAllocaBean) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		Integer count = customerAllocaBeanDao.findPagingCount(customerAllocaBean);
		List<CustomerAllocaBean> list = customerAllocaBeanDao.findPagingData(customerAllocaBean);
		map.put("code",0);
		map.put("msg","获取成功");
		map.put("count", count);
		map.put("data", list);
		return map;
	}
	
	@Override
	public List<CustomerAllocaBean> getTreeAll(CustomerAllocaBean customerAllocaBean) throws Exception {
		return customerAllocaBeanDao.findTreeData(customerAllocaBean);
	}

    public Map<String, Object> getCustomerAllocaInfo(CustomerAllocaBean customerAllocaBean) {
		Map<String, Object> map = new HashMap<String, Object>();
		Integer count = customerAllocaBeanDao.findCustomerPagingCount(customerAllocaBean);
		List<CustomerAllocaBean> list = customerAllocaBeanDao.findCustomerPagingData(customerAllocaBean);
		map.put("code",0);
		map.put("msg","获取成功");
		map.put("count", count);
		map.put("data", list);
		return map;
    }
}
