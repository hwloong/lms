package com.hbsoft.lms.dao.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.hb.service.INewService;
import com.hb.exception.SqlNotChangeException;
import com.hbsoft.lms.dao.BankDingTokenBeanDao;
import com.hbsoft.lms.bean.BankDingTokenBean;

@Service
public class BankDingTokenBeanDaoService implements INewService<BankDingTokenBean,Integer> {
	@Autowired
	private BankDingTokenBeanDao bankDingTokenBeanDao;

	@Override
	public Boolean add(BankDingTokenBean bankDingTokenBean) throws Exception {
		Integer i = bankDingTokenBeanDao.insert(bankDingTokenBean);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return true;
	}

	@Override
	public Integer addPrikey(BankDingTokenBean bankDingTokenBean) throws Exception {
		Integer prikey = null;
		Integer i = bankDingTokenBeanDao.insertPrikey(bankDingTokenBean);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		Object o = bankDingTokenBean.getId_prikey();
		if (o instanceof Integer) {
			prikey = (Integer) o;
		}
		return prikey;
	}

	@Override
	public Integer addBatch(List<BankDingTokenBean> list) throws Exception {
		Integer i = bankDingTokenBeanDao.insertBatch(list);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return i;
	}

	@Override
	public Integer remove(BankDingTokenBean bankDingTokenBean) throws Exception {
		Integer i = bankDingTokenBeanDao.delete(bankDingTokenBean);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return i;
	}

	@Override
	public Boolean removeOne(Integer id) throws Exception {
		Integer i = bankDingTokenBeanDao.deleteOne(id);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return true;
	}

	@Override
	public Integer removeBatch(List<Integer> ids) throws Exception {
		Integer i = bankDingTokenBeanDao.deleteBatch(ids);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return i;
	}

	@Override
	public Boolean set(BankDingTokenBean bankDingTokenBean) throws Exception {
		Integer i = bankDingTokenBeanDao.update(bankDingTokenBean);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public Boolean setEmpty(BankDingTokenBean bankDingTokenBean) throws Exception {
		Integer i = bankDingTokenBeanDao.updateEmpty(bankDingTokenBean);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public BankDingTokenBean getById(Integer id) throws Exception {
		return bankDingTokenBeanDao.findById(id);
	}

	@Override
	public BankDingTokenBean getByField(BankDingTokenBean bankDingTokenBean) throws Exception {
		return bankDingTokenBeanDao.findByField(bankDingTokenBean);
	}

	@Override
	public List<BankDingTokenBean> getAll(BankDingTokenBean bankDingTokenBean) throws Exception {
		return bankDingTokenBeanDao.findAll(bankDingTokenBean);
	}

	@Override
	public Map<String, Object> getPagingData(BankDingTokenBean bankDingTokenBean) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		Integer count = bankDingTokenBeanDao.findPagingCount(bankDingTokenBean);
		List<BankDingTokenBean> list = bankDingTokenBeanDao.findPagingData(bankDingTokenBean);
		map.put("code",0);
		map.put("msg","获取成功");
		map.put("count", count);
		map.put("data", list);
		return map;
	}
	
	@Override
	public List<BankDingTokenBean> getTreeAll(BankDingTokenBean bankDingTokenBean) throws Exception {
		return bankDingTokenBeanDao.findTreeData(bankDingTokenBean);
	}
}
