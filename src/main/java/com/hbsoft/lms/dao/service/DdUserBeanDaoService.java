package com.hbsoft.lms.dao.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.hb.service.INewService;
import com.hb.exception.SqlNotChangeException;
import com.hbsoft.lms.dao.DdUserBeanDao;
import com.hbsoft.lms.bean.DdUserBean;

@Service
public class DdUserBeanDaoService implements INewService<DdUserBean,Integer> {
	@Autowired
	private DdUserBeanDao ddUserBeanDao;

	@Override
	public Boolean add(DdUserBean ddUserBean) throws Exception {
		Integer i = ddUserBeanDao.insert(ddUserBean);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return true;
	}

	@Override
	public Integer addPrikey(DdUserBean ddUserBean) throws Exception {
		Integer prikey = null;
		Integer i = ddUserBeanDao.insertPrikey(ddUserBean);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		Object o = ddUserBean.getId_prikey();
		if (o instanceof Integer) {
			prikey = (Integer) o;
		}
		return prikey;
	}

	@Override
	public Integer addBatch(List<DdUserBean> list) throws Exception {
		Integer i = ddUserBeanDao.insertBatch(list);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return i;
	}

	@Override
	public Integer remove(DdUserBean ddUserBean) throws Exception {
		Integer i = ddUserBeanDao.delete(ddUserBean);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return i;
	}

	@Override
	public Boolean removeOne(Integer id) throws Exception {
		Integer i = ddUserBeanDao.deleteOne(id);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return true;
	}

	@Override
	public Integer removeBatch(List<Integer> ids) throws Exception {
		Integer i = ddUserBeanDao.deleteBatch(ids);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return i;
	}

	@Override
	public Boolean set(DdUserBean ddUserBean) throws Exception {
		Integer i = ddUserBeanDao.update(ddUserBean);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public Boolean setEmpty(DdUserBean ddUserBean) throws Exception {
		Integer i = ddUserBeanDao.updateEmpty(ddUserBean);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public DdUserBean getById(Integer id) throws Exception {
		return ddUserBeanDao.findById(id);
	}

	@Override
	public DdUserBean getByField(DdUserBean ddUserBean) throws Exception {
		return ddUserBeanDao.findByField(ddUserBean);
	}

	@Override
	public List<DdUserBean> getAll(DdUserBean ddUserBean) throws Exception {
		return ddUserBeanDao.findAll(ddUserBean);
	}

	@Override
	public Map<String, Object> getPagingData(DdUserBean ddUserBean) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		Integer count = ddUserBeanDao.findPagingCount(ddUserBean);
		List<DdUserBean> list = ddUserBeanDao.findPagingData(ddUserBean);
		map.put("code",0);
		map.put("msg","获取成功");
		map.put("count", count);
		map.put("data", list);
		return map;
	}
	
	@Override
	public List<DdUserBean> getTreeAll(DdUserBean ddUserBean) throws Exception {
		return ddUserBeanDao.findTreeData(ddUserBean);
	}
}
