package com.hbsoft.lms.dao.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.hb.service.INewService;
import com.hb.exception.SqlNotChangeException;
import com.hbsoft.lms.dao.BankBranchBeanDao;
import com.hbsoft.lms.bean.BankBranchBean;

@Service
public class BankBranchBeanDaoService implements INewService<BankBranchBean,Integer> {
	@Autowired
	private BankBranchBeanDao bankBranchBeanDao;

	@Override
	public Boolean add(BankBranchBean bankBranchBean) throws Exception {
		Integer i = bankBranchBeanDao.insert(bankBranchBean);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return true;
	}

	@Override
	public Integer addPrikey(BankBranchBean bankBranchBean) throws Exception {
		Integer prikey = null;
		Integer i = bankBranchBeanDao.insertPrikey(bankBranchBean);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		Object o = bankBranchBean.getId_prikey();
		if (o instanceof Integer) {
			prikey = (Integer) o;
		}
		return prikey;
	}

	@Override
	public Integer addBatch(List<BankBranchBean> list) throws Exception {
		Integer i = bankBranchBeanDao.insertBatch(list);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return i;
	}

	@Override
	public Integer remove(BankBranchBean bankBranchBean) throws Exception {
		Integer i = bankBranchBeanDao.delete(bankBranchBean);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return i;
	}

	@Override
	public Boolean removeOne(Integer id) throws Exception {
		Integer i = bankBranchBeanDao.deleteOne(id);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return true;
	}

	@Override
	public Integer removeBatch(List<Integer> ids) throws Exception {
		Integer i = bankBranchBeanDao.deleteBatch(ids);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return i;
	}

	@Override
	public Boolean set(BankBranchBean bankBranchBean) throws Exception {
		Integer i = bankBranchBeanDao.update(bankBranchBean);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public Boolean setEmpty(BankBranchBean bankBranchBean) throws Exception {
		Integer i = bankBranchBeanDao.updateEmpty(bankBranchBean);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public BankBranchBean getById(Integer id) throws Exception {
		return bankBranchBeanDao.findById(id);
	}

	@Override
	public BankBranchBean getByField(BankBranchBean bankBranchBean) throws Exception {
		return bankBranchBeanDao.findByField(bankBranchBean);
	}

	@Override
	public List<BankBranchBean> getAll(BankBranchBean bankBranchBean) throws Exception {
		return bankBranchBeanDao.findAll(bankBranchBean);
	}

	@Override
	public Map<String, Object> getPagingData(BankBranchBean bankBranchBean) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		Integer count = bankBranchBeanDao.findPagingCount(bankBranchBean);
		List<BankBranchBean> list = bankBranchBeanDao.findPagingData(bankBranchBean);
		map.put("code",0);
		map.put("msg","获取成功");
		map.put("count", count);
		map.put("data", list);
		return map;
	}
	
	@Override
	public List<BankBranchBean> getTreeAll(BankBranchBean bankBranchBean) throws Exception {
		return bankBranchBeanDao.findTreeData(bankBranchBean);
	}
}
