package com.hbsoft.lms.dao.service;

import com.hb.bean.CallResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.hb.service.INewService;
import com.hb.exception.SqlNotChangeException;
import com.hbsoft.lms.dao.NoticeBeanDao;
import com.hbsoft.lms.bean.NoticeBean;

@Service
public class NoticeBeanDaoService implements INewService<NoticeBean,Integer> {
	@Autowired
	private NoticeBeanDao noticeBeanDao;

	@Override
	public Boolean add(NoticeBean noticeBean) throws Exception {
		Integer i = noticeBeanDao.insert(noticeBean);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return true;
	}

	@Override
	public Integer addPrikey(NoticeBean noticeBean) throws Exception {
		Integer prikey = null;
		Integer i = noticeBeanDao.insertPrikey(noticeBean);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		Object o = noticeBean.getId_prikey();
		if (o instanceof Integer) {
			prikey = (Integer) o;
		}
		return prikey;
	}

	@Override
	public Integer addBatch(List<NoticeBean> list) throws Exception {
		Integer i = noticeBeanDao.insertBatch(list);
		if(i==0){
			throw new SqlNotChangeException("插入失败");
		}
		return i;
	}

	@Override
	public Integer remove(NoticeBean noticeBean) throws Exception {
		Integer i = noticeBeanDao.delete(noticeBean);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return i;
	}

	@Override
	public Boolean removeOne(Integer id) throws Exception {
		Integer i = noticeBeanDao.deleteOne(id);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return true;
	}

	@Override
	public Integer removeBatch(List<Integer> ids) throws Exception {
		Integer i = noticeBeanDao.deleteBatch(ids);
		if(i==0){
			throw new SqlNotChangeException("删除失败");
		}
		return i;
	}

	@Override
	public Boolean set(NoticeBean noticeBean) throws Exception {
		Integer i = noticeBeanDao.update(noticeBean);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public Boolean setEmpty(NoticeBean noticeBean) throws Exception {
		Integer i = noticeBeanDao.updateEmpty(noticeBean);
		if(i==0){
			throw new SqlNotChangeException("修改失败");
		}
		return true;
	}

	@Override
	public NoticeBean getById(Integer id) throws Exception {
		return noticeBeanDao.findById(id);
	}

	@Override
	public NoticeBean getByField(NoticeBean noticeBean) throws Exception {
		return noticeBeanDao.findByField(noticeBean);
	}

	@Override
	public List<NoticeBean> getAll(NoticeBean noticeBean) throws Exception {
		return noticeBeanDao.findAll(noticeBean);
	}

	@Override
	public Map<String, Object> getPagingData(NoticeBean noticeBean) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		Integer count = noticeBeanDao.findPagingCount(noticeBean);
		List<NoticeBean> list = noticeBeanDao.findPagingData(noticeBean);
		map.put("code",0);
		map.put("msg","获取成功");
		map.put("count", count);
		map.put("data", list);
		return map;
	}
	
	@Override
	public List<NoticeBean> getTreeAll(NoticeBean noticeBean) throws Exception {
		return noticeBeanDao.findTreeData(noticeBean);
	}

    public Map<String, Object> getNotice(NoticeBean noticeBean) throws Exception{
		Map<String, Object> map = new HashMap<String, Object>();
		Integer count = noticeBeanDao.findPagingCount(noticeBean);
		List<NoticeBean> list = noticeBeanDao.findNotice(noticeBean);
		map.put("code",0);
		map.put("msg","获取成功");
		map.put("count", count);
		map.put("data", list);
		return map;
    }

	public Map<String, Object> getNoticeExam(NoticeBean noticeBean) throws Exception{
		Map<String, Object> map = new HashMap<String, Object>();
		Integer count = noticeBeanDao.getNoticeExamCount(noticeBean);
		List<NoticeBean> list = noticeBeanDao.getNoticeExam(noticeBean);
		map.put("code",0);
		map.put("msg","获取成功");
		map.put("count", count);
		map.put("data", list);
		return map;
	}
}
