package com.hbsoft.lms.dao;

import org.apache.ibatis.annotations.Mapper;
import com.hb.dao.INewDao;
import com.hbsoft.lms.bean.AreaUserBean;

import java.util.List;

@Mapper
public interface AreaUserBeanDao extends INewDao<AreaUserBean,Integer> {

    List<AreaUserBean> findAllPagingData(AreaUserBean areaUserBean);

    Integer findAllPagingCount(AreaUserBean areaUserBean);
}
