package com.hbsoft.lms.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.omg.CORBA.OBJ_ADAPTER;

import java.util.Map;

@Mapper
public interface HBUserDao {

    Integer insertUser(Map<String,Object> param) throws Exception;

    Integer updateUser(Map<String, Object> param) throws Exception;

    Integer insertUserRole(@Param("roleId") Integer roleId, @Param("userId") Integer userId) throws Exception;

    Integer findUserNameCount(@Param("name") String name) throws Exception;

    Integer findRoleByAttr(@Param("attr") String attr) throws Exception;

    Integer sselectRoleByUserIdAndRoleId(Map<String,Object> map);

    Integer updateUserPass(Map<String, Object> param) throws Exception;

    Map<String,Object> selectRoleByUser(Map<String, Object> param) throws Exception;

    Integer delRoleByUserById(@Param("id_prikey")Integer id_prikey) throws Exception;
}
