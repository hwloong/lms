package com.hbsoft.lms.dao;

import org.apache.ibatis.annotations.Mapper;
import com.hb.dao.INewDao;
import com.hbsoft.lms.bean.BankBean;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface BankBeanDao extends INewDao<BankBean,Integer> {

    String selectMaxCode(@Param("areaCode") String areaCode);

    List<BankBean> getTreeAllByCode(@Param("param") String param);
}
