package com.hbsoft.lms.dao;

import org.apache.ibatis.annotations.Mapper;
import com.hb.dao.INewDao;
import com.hbsoft.lms.bean.DdDeptBean;

@Mapper
public interface DdDeptBeanDao extends INewDao<DdDeptBean,Integer> {
}
