package com.hbsoft.lms.dao;

import org.apache.ibatis.annotations.Mapper;
import com.hb.dao.INewDao;
import com.hbsoft.lms.bean.DdDeptAllBean;

@Mapper
public interface DdDeptAllBeanDao extends INewDao<DdDeptAllBean,Integer> {
}
