package com.hbsoft.lms.dao;

import org.apache.ibatis.annotations.Mapper;
import com.hb.dao.INewDao;
import com.hbsoft.lms.bean.HolidayBean;

@Mapper
public interface HolidayBeanDao extends INewDao<HolidayBean,Integer> {
}
