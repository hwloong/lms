package com.hbsoft.lms.dao;

import org.apache.ibatis.annotations.Mapper;
import com.hb.dao.INewDao;
import com.hbsoft.lms.bean.LoanUseBean;

@Mapper
public interface LoanUseBeanDao extends INewDao<LoanUseBean, Integer> {
	String selectMaxCode();
}
