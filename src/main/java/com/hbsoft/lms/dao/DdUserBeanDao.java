package com.hbsoft.lms.dao;

import org.apache.ibatis.annotations.Mapper;
import com.hb.dao.INewDao;
import com.hbsoft.lms.bean.DdUserBean;

@Mapper
public interface DdUserBeanDao extends INewDao<DdUserBean,Integer> {
}
