package com.hbsoft.lms.dao;

import org.apache.ibatis.annotations.Mapper;
import com.hb.dao.INewDao;
import com.hbsoft.lms.bean.CustomerAllocaBean;

import java.util.List;

@Mapper
public interface CustomerAllocaBeanDao extends INewDao<CustomerAllocaBean,Integer> {

    Integer findCustomerPagingCount(CustomerAllocaBean customerAllocaBean);

    List<CustomerAllocaBean> findCustomerPagingData(CustomerAllocaBean customerAllocaBean);
}
