package com.hbsoft.lms.dao;

import org.apache.ibatis.annotations.Mapper;
import com.hb.dao.INewDao;
import com.hbsoft.lms.bean.QrCodeBean;

@Mapper
public interface QrCodeBeanDao extends INewDao<QrCodeBean,Integer> {
}
