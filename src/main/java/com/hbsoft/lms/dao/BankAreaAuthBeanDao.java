package com.hbsoft.lms.dao;

import org.apache.ibatis.annotations.Mapper;
import com.hb.dao.INewDao;
import com.hbsoft.lms.bean.BankAreaAuthBean;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface BankAreaAuthBeanDao extends INewDao<BankAreaAuthBean,Integer> {
    List<BankAreaAuthBean> getAuthList(@Param("id") String id);
}
