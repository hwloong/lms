package com.hbsoft.lms.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface DdTreeDao {

	List<Map<String,Object>> getDdDeptTree(@Param("bankCode")String bankCode);
	
	List<Map<String,Object>> getDdUserTree(@Param("pid")String pid);
}
