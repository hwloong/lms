package com.hbsoft.lms.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hbsoft.lms.dao.StatisticsDao;
import com.hbsoft.lms.vo.StatisticsVo;

@Service
public class StatisticsService {

	@Autowired
	StatisticsDao statisticsDao;
	
	public Map<String,Object> statisticsCount(StatisticsVo vo) throws Exception {
		
		Integer bespeakCount = statisticsDao.bespeakCount(vo);
//		Integer bankAreaCount = statisticsDao.bankAreaCount();
//		Integer bankCount = statisticsDao.bankCount();
//		Integer managerCount = statisticsDao.managerCount();
		Integer adminAreaCount = statisticsDao.administratorCount("3",vo.getAreaCode());
		Integer adminBankCount = statisticsDao.administratorCount("6",vo.getAreaCode());
		Integer adminCount = statisticsDao.administratorCount(null,vo.getAreaCode());
		Integer creditCount = statisticsDao.creditCount();
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("bespeakCount", bespeakCount);
//		map.put("bankAreaCount", bankAreaCount);
//		map.put("bankCount", bankCount);
//		map.put("managerCount", managerCount);
		map.put("adminAreaCount", adminAreaCount);
		map.put("adminBankCount", adminBankCount);
		map.put("adminCount", adminCount);
		map.put("creditCount", creditCount);
	
		return map;
	}
	
	public Map<String,Object> bespeakFlow(StatisticsVo vo)throws Exception{
		Integer bespeakCount = statisticsDao.bespeakCount(vo);
		Map<String, Object> map = statisticsDao.bespeakFlow(vo);
		map.put("sum", bespeakCount);
		return map;
	}
	
	public List<Map<String,Object>> bespeakFlowCityRank() throws Exception{
		List<Map<String, Object>> sumCity = statisticsDao.bespeakCityRank();
		List<Map<String, Object>> flowCity = statisticsDao.bespeakFlowCityRank();
		for(Map flow: flowCity){
			for(Map sum:sumCity){
				String city1 = (String)sum.get("name");
				String city2 = (String)flow.get("name");
				if(city1.equals(city2)){
					flow.put("sum", sum.get("num"));
					continue;
				}
			}
		}
		Collections.sort(flowCity, new Comparator<Map<String, Object>>() {
			   @Override
			   public int compare(Map<String, Object> o1, Map<String, Object> o2) {
				   Integer sum1 =  (Integer)o1.get("sum");
				   Integer sum2 =  (Integer)o2.get("sum");
			      return sum2.compareTo(sum1);
			   }
			});
		return flowCity;
	}
	
	public List<Map<String,Object>> bespeakFlowBankRank(StatisticsVo vo)throws Exception{
		List<Map<String, Object>> sumBank = statisticsDao.bespeakBankRank(vo);
		List<Map<String, Object>> flowBank = statisticsDao.bespeakFlowBankRank(vo);
		for(Map flow: flowBank){
			for(Map sum:sumBank){
				String city1 = (String)sum.get("name");
				String city2 = (String)flow.get("name");
				if(city1.equals(city2)){
					flow.put("sum", sum.get("num"));
					continue;
				}
			}
		}
		Collections.sort(flowBank, new Comparator<Map<String, Object>>() {
			   @Override
			   public int compare(Map<String, Object> o1, Map<String, Object> o2) {
				   Integer sum1 =  (Integer)o1.get("sum");
				   Integer sum2 =  (Integer)o2.get("sum");
			      return sum2.compareTo(sum1);
			   }
			});
		return flowBank;
	}
	
	public List<Map<String,Object>> managerBankRank(StatisticsVo vo)throws Exception{
		List<Map<String, Object>> managerBankRank = statisticsDao.managerBankRank(vo);
		return managerBankRank;
	}
	
	public Integer managerBankPageCount(StatisticsVo vo)throws Exception{
		return statisticsDao.managerBankPageCount(vo);
	}
	
	public List<Map<String,Object>> creditCtiyRank(StatisticsVo vo)throws Exception{
		List<Map<String, Object>> list = statisticsDao.creditCtiyRank(vo);
		return list;
	}
	
	public Integer creditCtiyPageCount()throws Exception{
		return statisticsDao.creditCtiyPageCount();
	}
	
	public List<Map<String,Object>> creditBankRank(StatisticsVo vo)throws Exception{
		List<Map<String, Object>> list = statisticsDao.creditBankRank(vo);
		return list;
	}
	
	public Integer creditBankPageCount()throws Exception{
		return statisticsDao.creditBankPageCount();
	}

	public String getAreaCodeByName(String name)throws Exception{
		return statisticsDao.getAreaCodeByName(name);
	}
}
