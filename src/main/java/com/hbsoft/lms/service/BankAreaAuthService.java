package com.hbsoft.lms.service;

import com.hb.bean.CallResult;
import com.hbsoft.lms.bean.BankAreaBean;
import com.hbsoft.lms.bean.BankBean;
import com.hbsoft.lms.dao.service.AreaUserBeanDaoService;
import com.hbsoft.lms.dao.service.BankAreaAuthBeanDaoService;
import com.hbsoft.lms.dao.service.BankAreaBeanDaoService;
import com.hbsoft.lms.dao.service.BankBeanDaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class BankAreaAuthService {

    @Autowired
    BankAreaAuthBeanDaoService bankAreaAuthBeanDaoService;

    @Autowired
    AreaUserBeanDaoService areaUserBeanDaoService;

    @Autowired
    BankAreaBeanDaoService bankAreaBeanDaoService;

    @Autowired
    BankBeanDaoService bankBeanDaoService;

    @Transactional
    public CallResult getBankAreaAllTree(String param) {
        CallResult result = null;
        if (null == param || param.equals("")) {
            result = new CallResult<>();
            List<BankAreaBean> list = bankAreaBeanDaoService.getTreeByCode(param);
            if (null != list && list.size() > 0) {
                 for (BankAreaBean data : list) {
                     if (data.getCode().length() < 4) {
                         data.setParent(true);
                     }
                 }
                 result.setCode(0);
                 result.setMessage("获取成功");
                 result.setData(list);
            }else {
                result.setCode(400);
                result.setMessage("暂无数据");
                result.setData(list);
            }
        }else if (param.length() < 4){
            result = new CallResult<>();
            List<BankBean> list = bankBeanDaoService.getTreeAllByCode(param);
            if (list != null && list.size() > 0) {
                for (BankBean data : list) {
                    data.setPId(param);
                }
                result.setCode(0);
                result.setMessage("获取成功");
                result.setData(list);
            }else {
                result.setCode(400);
                result.setMessage("暂无数据");
                result.setData(list);
            }
        }
        return result;
    }
}
