package com.hbsoft.lms.service.impl;

import com.hb.bean.CallResult;
import com.hbsoft.lms.bean.NoticeBean;
import com.hbsoft.lms.dao.service.NoticeBeanDaoService;
import com.hbsoft.lms.service.NoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service("NoticeService")
public class NoticeServiceImpl implements NoticeService {

    @Autowired
    NoticeBeanDaoService noticeBeanDaoService;


    @Override
    public Map<String, Object> getNotice(NoticeBean noticeBean) throws Exception {
        Map<String, Object> map = noticeBeanDaoService.getNotice(noticeBean);
        return map;
    }

    @Override
    public Map<String, Object> getNoticeExam(NoticeBean noticeBean) throws Exception {
        return noticeBeanDaoService.getNoticeExam(noticeBean);
    }
}
