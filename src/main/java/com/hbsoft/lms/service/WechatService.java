package com.hbsoft.lms.service;

import com.hb.bean.CallResult;
import com.hbsoft.common.enumtype.QrCodeEnum;
import com.hbsoft.lms.bean.BankBranchBean;
import com.hbsoft.lms.bean.QrCodeBean;
import com.hbsoft.lms.dao.service.BankBranchBeanDaoService;
import com.hbsoft.lms.dao.service.QrCodeBeanDaoService;
import com.hbsoft.util.WechatUtil;
import org.omg.CORBA.OBJ_ADAPTER;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ResourceUtils;

import java.net.URLEncoder;
import java.util.Date;
import java.util.Map;

@Service
public class WechatService {

    @Autowired
    WechatUtil wechatUtil;

    @Autowired
    BankBranchBeanDaoService bankBranchBeanDaoService;

    @Autowired
    QrCodeBeanDaoService qrCodeBeanDaoService;

    //private final static String GETQRCODEBYTICKTURL = "https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=TICKET";

    @Transactional
    public CallResult<QrCodeBean> makeQrCode(String qrCodeValue,String qrCodeType,String qrCodeName) throws  Exception{
        CallResult<QrCodeBean> result = new CallResult<>();
       /* BankBranchBean bb = bankBranchBeanDaoService.getById(id_prikey);
        if(bb == null){
            result.setCode(400);
            result.setMsg("部门参数有误");
            return result;
        }*/
        QrCodeBean param = new QrCodeBean();
        param.setId(qrCodeValue);
        QrCodeBean qrc = qrCodeBeanDaoService.getByField(param);
        if(qrc != null){
            result.setCode(0);
            result.setMsg("操作成功");
            result.setData(qrc);
            return result;
        }
        Map<String, Object> map= wechatUtil.getQrCode(qrCodeType + qrCodeValue);
        if(map == null){
            result.setCode(400);
            result.setMsg("二维码生成失败");
            return result;
        }
        String ticket = (String) map.get("ticket");
        String url = (String) map.get("url");
      /*  String qrCodePath = qrCodeValue;
        if(qrCodePath != null && qrCodePath.length()>3){
            qrCodePath = qrCodePath.substring(0,3);
        }
        String imgPath = "img/qrCode/" + qrCodePath + "/" + qrCodeName + ".jpg";
        String path =  ResourceUtils.getURL("classpath:").getPath()+ "/static/" +imgPath;
        String ticktUrl = GETQRCODEBYTICKTURL.replace("TICKET", URLEncoder.encode(ticket,"utf-8"));
        Boolean flag = wechatUtil.sendJSONPostReturnInputStrem(ticktUrl,null,path,null,"utf-8",5000,5000);
        if(flag){*/
            QrCodeBean record = new QrCodeBean();
            record.setId(qrCodeValue);
            record.setType(QrCodeEnum.QR_CODE_BANKBRANCH.getName());
            record.setTicket(ticket);
            record.setUrl(url);
            //record.setPath(imgPath);
            record.setCreateOn(new Date());
            qrCodeBeanDaoService.add(record);
            result.setCode(0);
            result.setMsg("操作成功");
            result.setData(record);
        /*}else{
            result.setCode(400);
            result.setMsg("图片生成失败");
        }*/

        return result;
    }
}
