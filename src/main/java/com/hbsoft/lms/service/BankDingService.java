package com.hbsoft.lms.service;

import com.hb.bean.CallResult;
import com.hbsoft.common.enumtype.DingInterfaceEnum;
import com.hbsoft.lms.bean.BankDingTokenBean;
import com.hbsoft.lms.dao.service.BankDingTokenBeanDaoService;
import com.hbsoft.lms.vo.DDUserVo;
import com.hbsoft.lms.vo.DepartmentVo;
import com.hbsoft.util.DDUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BankDingService {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private BankDingTokenBeanDaoService bankDingTokenBeanDaoService;

    @Autowired
    private DDUtil ddUtil;

    public CallResult<DepartmentVo> getBankDingDeptFromInterface(String bankCode) throws Exception {
        CallResult<DepartmentVo> result = new CallResult<>();
        if(StringUtils.isBlank(bankCode)){
            result.setCode(400);
            result.setMsg("参数有误");
            return result;
        }
            BankDingTokenBean param = new BankDingTokenBean();
            param.setBankCode(bankCode);
            BankDingTokenBean bankDingTokenBean = bankDingTokenBeanDaoService.getByField(param);
            if(bankDingTokenBean == null || StringUtils.isBlank(bankDingTokenBean.getServiceURL()) || StringUtils.isBlank(bankDingTokenBean.getServiceMethod())){
                result.setCode(400);
                result.setMsg("请先配置钉钉基本信息");
                return result;
            }
            DepartmentVo departmentVo = null;
            if(DingInterfaceEnum.DING_INTERFACE_ENUM_DING.getCode().equals(bankDingTokenBean.getTokenMode())){
                departmentVo = ddUtil.getDepts(bankDingTokenBean.getServiceURL(),bankDingTokenBean.getServiceParas());
            }else if(DingInterfaceEnum.DING_INTERFACE_ENUM_HB.getCode().equals(bankDingTokenBean.getTokenMode())){
                departmentVo = ddUtil.getDeptsByHbInterface(bankDingTokenBean.getServiceURL(),bankDingTokenBean.getServiceMethod());
            }
            if(departmentVo != null){
                if( "0".equals(departmentVo.getErrcode())){
                    result.setCode(0);
                    result.setMsg(departmentVo.getErrmsg());
                    result.setData(departmentVo);
                }else{
                    result.setCode(400);
                    result.setMsg(departmentVo.getErrmsg());
                }
            }
        return  result;
    }

    public CallResult<DDUserVo> getBankDingUserFromInterface(String bankCode, String deptId) throws Exception {
        CallResult<DDUserVo> result = new CallResult<>();
        if(StringUtils.isBlank(bankCode)){
            result.setCode(400);
            result.setMsg("参数有误");
            return result;
        }
            BankDingTokenBean param = new BankDingTokenBean();
            param.setBankCode(bankCode);
            BankDingTokenBean bankDingTokenBean = bankDingTokenBeanDaoService.getByField(param);
            if(bankDingTokenBean == null || StringUtils.isBlank(bankDingTokenBean.getServiceURL()) || StringUtils.isBlank(bankDingTokenBean.getServiceMethod())){
                result.setCode(400);
                result.setMsg("请先配置钉钉基本信息");
                return result;
            }
            DDUserVo ddUserVo = null;
            if(DingInterfaceEnum.DING_INTERFACE_ENUM_DING.getCode().equals(bankDingTokenBean.getTokenMode())){
                ddUserVo = ddUtil.getUsers(bankDingTokenBean.getServiceURL(),bankDingTokenBean.getServiceParas(),deptId);
            }else if(DingInterfaceEnum.DING_INTERFACE_ENUM_HB.getCode().equals(bankDingTokenBean.getTokenMode())){
                ddUserVo = ddUtil.getUserByHbInterface(bankDingTokenBean.getServiceURL(),bankDingTokenBean.getServiceMethod(),deptId);
            }
            if(ddUserVo != null){
                if("0".equals(ddUserVo.getErrcode())){
                    result.setCode(0);
                    result.setMsg(ddUserVo.getErrmsg());
                    result.setData(ddUserVo);
                }else{
                    result.setCode(400);
                    result.setMsg(ddUserVo.getErrmsg());
                }
            }
        return  result;
    }
}
