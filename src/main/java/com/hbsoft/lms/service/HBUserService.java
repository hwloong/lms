package com.hbsoft.lms.service;

import com.hb.bean.CallResult;
import com.hb.util.BasePassUtil;
import com.hbsoft.lms.dao.HBUserDao;
import com.sun.org.apache.bcel.internal.generic.IF_ACMPEQ;
import org.omg.CORBA.OBJ_ADAPTER;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

@Service
public class HBUserService {

    @Autowired
    private HBUserDao hbUserDao;

    @Transactional
    public CallResult<String> addHBUser(String mobile, String name, String passWord,String roleAttr) throws  Exception{
        CallResult<String> result = new CallResult<>();
        Integer roleId =  hbUserDao.findRoleByAttr(roleAttr);
        if(roleId != null){
            String pass = BasePassUtil.passTransformation(mobile, passWord);
            Map<String, Object> param = new HashMap<>();
            param.put("name", mobile);
            param.put("pass", pass);
            param.put("describe", name);
            Integer user_id_prikey = hbUserDao.findUserNameCount(mobile);
            if ( user_id_prikey == null  ) {
                Integer userId = null;
                Integer i = hbUserDao.insertUser(param);
                if (i == 1) {
                    userId = (Integer) param.get("id_prikey");
                    if (userId != null) {
                        hbUserDao.insertUserRole(roleId, userId);
                    }
                }
            }else{
               // param.put("id_prikey", user_id_prikey);
               // hbUserDao.updateUser(param);
                Map<String, Object> mapParam = new HashMap<>();
                mapParam.put("roleId",roleId);
                mapParam.put("userId",user_id_prikey);
                Integer count = hbUserDao.sselectRoleByUserIdAndRoleId(mapParam);
                if(count == null || count == 0){
                    hbUserDao.insertUserRole(roleId, user_id_prikey);
                }
            }
        }else {
            result.setCode(400);
            result.setMsg("请联系管理员添加角色");
        }
        return result;
    }

    @Transactional
    public CallResult<String> setHBuser(String mobile,String password) throws Exception {
        CallResult<String> result = new CallResult<>();
        if (null != mobile && !mobile.equals("")) {
            String pass = BasePassUtil.passTransformation(mobile, password);
            Map<String, Object> param = new HashMap<>();
            param.put("pass", pass);
            Integer user_id_prikey = hbUserDao.findUserNameCount(mobile);
            if (null == user_id_prikey) {
                result.setCode(400);
                result.setMessage("参数有误");
            }else {
                param.put("id_prikey", user_id_prikey);
                hbUserDao.updateUserPass(param);
                result.setCode(0);
                result.setMessage("修改成功");
            }
        }else {
            result.setCode(400);
            result.setMessage("参数有误");
        }
        return result;
    }
}
