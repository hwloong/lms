package com.hbsoft.lms.service;

import com.hb.bean.CallResult;
import com.hbsoft.lms.bean.NoticeBean;

import java.util.List;
import java.util.Map;

public interface NoticeService {

    /**
     * 获取发布公告
     * @return
     * @throws Exception
     */
    Map<String, Object> getNotice(NoticeBean noticeBean) throws Exception;

    /**
     * 获取公告审核列表
     * @return
     * @throws Exception
     */
    Map<String, Object> getNoticeExam(NoticeBean noticeBean) throws Exception;
}
