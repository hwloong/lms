package com.hbsoft.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;


@Component
@Setter
@Getter
public class SysConfigInfo {

	@Value("${hb.taskFlag:false}")
	private boolean taskFlag;

	public static String uploadPath;

	@Value("${hb.uploadPath:}")
	public void setUploadPath(String uploadPath) {
		SysConfigInfo.uploadPath = uploadPath;
	}


	@Value("${hb.dd.ddTokenInterfacePath:}")
	private String ddTokenInterfacePath;

	@Value("${hb.wx.wxTokenInterfacePath:}")
	private String wxTokenInterfacePath;

	@Value("${hb.dd.ddDeptInterfacePath:}")
	private String ddDeptInterfacePath;

	@Value("${hb.dd.corpID}")
	private String corpID;

}
