package com.hbsoft.util;

import com.hb.util.BasePassUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Service
public class VolidateSignUtil {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    public  Object objectDecode(Object o){
        String objectStr = GsonUtil.toJson(o);
        Map<String,String> map = GsonUtil.syncFromJson(objectStr,new HashMap<>().getClass());
        for (Map.Entry<String, String> entry : map.entrySet()) {
            map.put(entry.getKey(), BasePassUtil.base64_decode(entry.getValue()));
        }
        String mapStr = GsonUtil.toJson(map);
        logger.info("解密后数据：{}",mapStr);
        Object obj = GsonUtil.fromJson(mapStr,o.getClass());
        return obj;
    }


    public  Boolean volidateSign(Object o,String signStr){
        try {
            String objectStr = GsonUtil.toJson(o);
            logger.info("签名验证，生成json:{}",objectStr);
            Map<String,String> map = GsonUtil.syncFromJson(objectStr,new HashMap<>().getClass());
            for (Map.Entry<String, String> entry : map.entrySet()) {
                map.put(entry.getKey(), BasePassUtil.base64_decode(entry.getValue()));
            }
            String sign = ApiSign.signTopRequest(map,null,"hbsoft","md5");
            logger.info("签名验证，生成签名：{}",sign);
            if(sign.equals(signStr)){
                return true;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }
}
