package com.hbsoft.util;

import net.iharder.Base64;

import java.security.MessageDigest;

public class V1Pass {
	private static String one = "13@1709";

	/**
	 * 加密
	 * 
	 * @param data
	 * @return
	 */
	public static String pass_encode(String data) {
		String str = null;
		if ((data != null) && (!"".equals(data))) {
			try {
				String code = base64_encode(data);
				char[] chars = code.toCharArray();
				int num = 0;
				char[] arrayOfChar1;
				int j = (arrayOfChar1 = chars).length;
				for (int i = 0; i < j; i++) {
					char c = arrayOfChar1[i];
					num += c;
				}
				String key = null;
				if (num < 100) {
					key = String.format("%03d", new Object[] { Integer.valueOf(num) });
				} else {
					key = num + "";
					key = key.substring(key.length() - 3);
				}
				String pass = pass(code, key);
				StringBuffer sb = new StringBuffer();
				sb.append(key.substring(2));
				sb.append(pass.substring(0, 1));
				sb.append(key.substring(1, 2));
				sb.append(pass.substring(1, 2));
				sb.append(key.substring(0, 1));
				sb.append(pass.substring(2));
				str = "v1" + base64_encode(pass(sb.toString(), one));
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * 解密
	 * 
	 * @param data
	 * @return
	 */
	public static String pass_decode(String data) {
		String str = null;
		if ((data != null) && (!"".equals(data))) {
			try {
				data = data.substring(2);
				String pass = pass(base64_decode(data), one);
				String key = pass.substring(4, 5) + pass.substring(2, 3) + pass.substring(0, 1);
				pass = pass.substring(1, 2) + pass.substring(3, 4) + pass.substring(5);
				pass = pass(pass, key);
				str = base64_decode(pass);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	private static String pass(String data, String key) {
		if ((key == null) || ("".equals(key))) {
			key = "9DSOFT";
		}
		int j = 1;
		byte a = 0;
		byte b = 0;
		StringBuffer sb = new StringBuffer();
		for (int i = 1; i <= data.length(); i++) {
			if (j > key.length()) {
				j = 1;
			}
			a = data.substring(i - 1, i).getBytes()[0];
			b = key.substring(j - 1, j).getBytes()[0];
			sb.append((char) (a ^ b));
			j++;
		}
		return sb.toString();
	}

	private static String base64_decode(String str) throws Exception {
		return new String(Base64.decode(str), "gbk");
	}

	private static String base64_encode(String str) throws Exception {
		return Base64.encodeBytes(str.getBytes("gbk"));
	}

	public static String md5(String value) throws Exception {
		MessageDigest md = MessageDigest.getInstance("MD5");
		md.update(value.getBytes());
		byte[] b = md.digest();

		StringBuffer buf = new StringBuffer("");
		for (int offset = 0; offset < b.length; offset++) {
			int i = b[offset];
			if (i < 0) {
				i += 256;
			}
			if (i < 16) {
				buf.append("0");
			}
			buf.append(Integer.toHexString(i));
		}
		return buf.toString();
	}
	public static void main(String[] args) {

		try {


			System.out.println(V1Pass.pass_decode("v1B05wVQdXfk9BMXhMVGI1TQpGf01LRHo4RUoy"));
			System.out.println(V1Pass.pass_decode("v1Ak14XQRWc0R6IjtKR1h6TyxpOQ=="));


		/*	v1B05wVQdXfk9BMXhMVGI1TQpGf01LRHo4RUoy
			v1B0x2UwVhOUhRGTJOVmgzTjJgOA==*/

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
