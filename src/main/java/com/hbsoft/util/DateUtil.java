package com.hbsoft.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class DateUtil {

    /**
     * 时间加减天数
     * @param startDate 要处理的时间，Null则为当前时间
     * @param days 加减的天数
     * @return Date
     */
    public static Date dateAddDays(Date startDate, int days) {
        if (startDate == null) {
            startDate = new Date();
        }
        Calendar c = Calendar.getInstance();
        c.setTime(startDate);
        c.set(Calendar.DATE, c.get(Calendar.DATE) + days);
        return c.getTime();
    }

    /**
     * 时间加减周
     * @param startDate 要处理的时间，Null则为当前时间
     * @param weeks 加减的天数
     * @return Date
     */
    public static Date dateWeekDays(Date startDate, int weeks) {
        if (startDate == null) {
            startDate = new Date();
        }
        Calendar c = Calendar.getInstance();
        c.setTime(startDate);
        c.set(Calendar.WEEK_OF_YEAR, c.get(Calendar.WEEK_OF_YEAR) + weeks);
        return c.getTime();
    }

    /**
     * 时间加减月数
     * @param startDate 要处理的时间，Null则为当前时间
     * @param months 加减的月数
     * @return Date
     */
    public static Date dateAddMonths(Date startDate, int months) {
        if (startDate == null) {
            startDate = new Date();
        }
        Calendar c = Calendar.getInstance();
        c.setTime(startDate);
        c.set(Calendar.MONTH, c.get(Calendar.MONTH) + months);
        return c.getTime();
    }

    /**
     * 时间加减年
     * @param startDate
     * @param years
     * @return
     */
    public static Date dateAddYears(Date startDate, int years) {
        if (startDate == null) {
            startDate = new Date();
        }
        Calendar c = Calendar.getInstance();
        c.setTime(startDate);
        c.add(Calendar.YEAR, years);
        return c.getTime();
    }

    /**
     * 时间转字符串
     * @param date
     * @param format
     * @return
     */
    public static String dateFormatString(Date date, String format) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        return dateFormat.format(date);
    }

    /**
     * 获取指定时间格式
     * @param date
     * @param format
     * @return
     */
    public static Date dateFormatDate(Date date, String format){
        String dateStr =  dateFormatString(date,format);
        return stringFormatDate(dateStr,format);
    }

    /**
     * 获取当前时间指定的格式
     * @param format
     * @return
     */
    public static Date getDate(String format){
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        String dateStr = dateFormat.format(new Date());
        try {
            return dateFormat.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 字符串转时间
     * @param date
     * @param format
     * @return
     */
    public static Date stringFormatDate(String date, String format){
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        try {
            return dateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 获取连续时间
     * @param startDate
     * @param endDate
     * @return
     */
    public static List getContinuityDay(Date startDate,Date endDate){
        List<String> list = new ArrayList<String>();
        Date date = startDate;
        while (date.compareTo(endDate)<0){
            Calendar c = Calendar.getInstance();
            c.setTime(date);
            list.add(String.valueOf(c.get(Calendar.DAY_OF_MONTH)));
            date = dateAddDays(date,1);
        }
        return  list;
    }

    /**
     * 获取连续的月
     * @param startDate
     * @param endDate
     * @return
     */
    public static List getContinuityMonth(Date startDate,Date endDate){
        List<String> list = new ArrayList<String>();
        Date date = startDate;
        while (date.compareTo(endDate)<0){
            Calendar c = Calendar.getInstance();
            c.setTime(date);
            list.add(String.valueOf(c.get(Calendar.MONTH)+1));
            date = dateAddMonths(date,1);
        }
        return  list;
    }

    /**
     * 获取连续的年
     * @param startDate
     * @param endDate
     * @return
     */
    public static List getContinuityYear(Date startDate,Date endDate){
        List<String> list = new ArrayList<String>();
        Date date = startDate;
        while (date.compareTo(endDate)<0){
            Calendar c = Calendar.getInstance();
            c.setTime(date);
            list.add(String.valueOf(c.get(Calendar.YEAR)));
            date = dateAddYears(date,1);
        }
        return  list;
    }

    /**
     * 获取月份的第一天
     * @param date
     * @return
     */
    public  static Date getMonthOfFirstDay(Date date){
        Calendar c = Calendar.getInstance();
        if (date != null) {
            c.setTime(date);
        }
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        c.set(Calendar.DAY_OF_MONTH, 1);
        return c.getTime();
    }

    /**
     * 获取月份的最后一天
     * @param date
     * @return
     */
    public static Date getMonthOfLastDay(Date date){
        Calendar c = Calendar.getInstance();
        if (date != null) {
            c.setTime(date);
        }
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        c.set(Calendar.DAY_OF_MONTH,c.getActualMaximum(Calendar.DAY_OF_MONTH));

        return c.getTime();
    }

    /***
     * @comments 计算两个时间的时间差
     * @param strTime1
     * @param strTime2
     */
    public static String getTimeDifference(String strTime1,String strTime2) {
        //格式日期格式，在此我用的是"2018-01-24 19:49:50"这种格式
        //可以更改为自己使用的格式，例如：yyyy/MM/dd HH:mm:ss 。。。
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dHms = "";
        try{
            Date now = df.parse(strTime1);
            Date date=df.parse(strTime2);
            long l=now.getTime()-date.getTime();       //获取时间差
            long day=l/(24*60*60*1000);
            long hour=(l/(60*60*1000)-day*24);
            long min=((l/(60*1000))-day*24*60-hour*60);
            long s=(l/1000-day*24*60*60-hour*60*60-min*60);
            dHms = ""+day+"天"+hour+"小时"+min+"分"+s+"秒";
        }catch(Exception e){
            e.printStackTrace();
        }
        return dHms;
    }

    public static void main(String[] args) {
       /* System.out.println(dateFormatString(DateUtil.dateAddDays(new Date(),-7),"yyyy-MM-dd"));
        System.out.println(dateFormatString(DateUtil.dateWeekDays(new Date(),-1),"yyyy-MM-dd"));
        System.out.println(dateFormatString(DateUtil.dateAddMonths(new Date(),-1),"yyyy-MM-dd"));*/

  /*     Date date = getDate("yyyy-MM-dd");
       List list =  getContinuityMonth(dateAddMonths(date,-6),date);
        System.out.println(list);*/
       /* System.out.println(getMonthOfFirstDay(null));
        System.out.println(getMonthOfLastDay(null));*/
        String strTime1 = "2020-02-07 19:49:50";
        String strTime2 = "2017-01-07 10:58:40";
        System.out.println(DateUtil.getTimeDifference(strTime1,strTime2));
    }


}
