package com.hbsoft.util;

import com.google.gson.reflect.TypeToken;
import com.hb.util.HttpUtil;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class HolidayUtil {

    public static final String HOLIDAY_TIME = "http://timor.tech/api/holiday/info/";
    /**
     * 得到指定区间内的所有日期
     * @param startTime
     * @param endTime
     * @return
     */
    public static List<String> getAllDays(String startTime, String endTime) {
        List<String> days = new ArrayList<>();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date start = dateFormat.parse(startTime);
            Date end = dateFormat.parse(endTime);

            Calendar tempStart = Calendar.getInstance();
            tempStart.setTime(start);

            Calendar tempEnd = Calendar.getInstance();
            tempEnd.setTime(end);
            tempEnd.add(Calendar.DATE,+1);
            while(tempStart.before(tempEnd)) {
                String formatLineDay = dateFormat.format(tempStart.getTime());
//                String formatDat = formatLineDay.replaceAll("-","");
                days.add(formatLineDay);
                tempStart.add(Calendar.DAY_OF_YEAR,1);
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
        return days;
    }


    /**
     * 获取指定日期区间的节假日
     * @param httpArgs
     * @return
     */
    public static List<Map<String, Object>> holidayCopy(List<String> httpArgs) {
        String result = "";
        List<Map<String, Object>> list = new ArrayList<>();
        List<Map<String, Object>> list1 = new ArrayList<>();
        try {
            for (int i=0; i<httpArgs.size(); i++) {
                result = HttpUtil.sendGet(HOLIDAY_TIME + httpArgs.get(i),null,null,null,200000);
                Map<String, Object> map = GsonUtil.fromJson(result,new TypeToken<Map<String, Object>>(){}
                .getType());
                Map<String, Object> map1 = GsonUtil.fromJson(GsonUtil.toJson(map.get("type")),new TypeToken<Map<String, Object>>(){}
                .getType());
                map1.put("holiday",httpArgs.get(i));
                list.add(map1);
            }
            for (int j=0; j<list.size(); j++) {
                if (list.get(j).get("type").toString().equals("1.0")) {
                    list.get(j).put("type","假日");
                    list1.add(list.get(j));
                }else if (list.get(j).get("type").toString().equals("2.0")) {
                    list.get(j).put("type","节日");
                    list1.add(list.get(j));
                }else {
                    continue;
                }
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
        return list1;
    }

    public static void main(String args[]) {
        List<String> days = getAllDays("2020-01-01","2020-01-10");
        List<Map<String, Object>> list = holidayCopy(days);
        for(int i=0; i<list.size(); i++) {
            System.out.println(list.get(i).get("type")+" "+list.get(i).get("holiday"));
        }
    }
}
