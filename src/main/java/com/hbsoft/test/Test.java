package com.hbsoft.test;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

public class Test {




	public static void main(String[] args) throws Exception {
		// 网络附件url
		String path = "http://sq.vfpbs.com/static/1.txt";
		//保存到本地F盘
		String name = "F:\\1.txt";

		URL url = new URL(path);
		//链接url
		URLConnection uc = url.openConnection();
		//获取输入流
		InputStream in = uc.getInputStream();
		byte[] data = readInputStream(in);
		File file = new File(name);
		FileOutputStream outputStream = null;
		try {
			outputStream = new FileOutputStream(file);
			outputStream.write(data);
		} catch (Exception e) {

		} finally {
			outputStream.close();
		}
	}

	public static byte[] readInputStream(InputStream inStream) throws Exception {
		ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		//创建一个Buffer字符串
		byte[] buffer = new byte[1024];
		//每次读取的字符串长度，如果为-1，代表全部读取完毕
		int len = 0;
		//使用一个输入流从buffer里把数据读取出来
		while ((len = inStream.read(buffer)) != -1) {
			//用输出流往buffer里写入数据，中间参数代表从哪个位置开始读，len代表读取的长度
			outStream.write(buffer, 0, len);
		}
		//关闭输入流
		inStream.close();
		//把outStream里的数据写入内存
		return outStream.toByteArray();
	}

}

