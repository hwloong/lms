import com.hb.bean.CallResult;
import com.hbsoft.Application;
import com.hbsoft.lms.bean.DdDeptBean;
import com.hbsoft.lms.bean.DdUserBean;
import com.hbsoft.lms.dao.service.DdDeptBeanDaoService;
import com.hbsoft.lms.service.BankDingService;
import com.hbsoft.lms.service.CustomerManagerService;
import com.hbsoft.lms.service.WechatService;
import com.hbsoft.lms.vo.DDUserVo;
import com.hbsoft.lms.vo.DepartmentVo;
import com.hbsoft.util.WechatUtil;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.util.List;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class Test {


    @Autowired
    private DdDeptBeanDaoService ddDeptBeanDaoService;

    @Autowired
    private CustomerManagerService customerManagerService;

    @Autowired
    private WechatService wechatService;

    @Autowired
    private BankDingService bankDingService;

    @org.junit.Test
    public  void test2() throws  Exception{
        String path =  ResourceUtils.getURL("classpath:").getPath()+"static/qrcode/"+"s.txt";
        File file = new File(path);
        if (!file.exists()) {
            file.createNewFile();
        }
    }

    @org.junit.Test
    public void test3(){
        try {
            customerManagerService.updateDdInfo("901",null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @org.junit.Test
    public void test4(){
        try {
           // CallResult<DepartmentVo> result1 = bankDingService.getBankDingDeptFromInterface("302");
            CallResult<DDUserVo> result2 = bankDingService.getBankDingUserFromInterface("302","90403080");
            System.out.println("");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
